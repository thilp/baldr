package BaldrLogger;
use 5.010_001;
use strict;
use warnings;

use Carp;
use Term::ANSIColor qw(:constants colorstrip);

use parent 'Baldr::ILogger';

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);
    my $prompt = CYAN . BOLD . 'BALDR' . RESET . ': ';
    $self->{prompt} = -t *STDOUT ? $prompt : colorstrip($prompt);
    return $self;
}

sub log {
    my $self = shift;
    return unless $self->active;
    @_ = map { colorstrip($_) } @_ unless $self->getIsatty;
    chomp(my $last = $_[-1]);
    say {$self->{output}} $self->{prompt}, @_[ 0 .. $#_ - 1], $last;
}

sub die {
    my $self = shift;
    @_ = map { colorstrip($_) } @_ unless $self->getIsatty;
    chomp(my $last = $_[-1]);
    confess $self->{prompt}, @_[ 0 .. $#_ - 1], $last;
}

1;
