package Baldr;

use 5.010_001;
use strict;
use warnings;
use autodie;

our $VERSION = '1.0';

use Carp;
use File::Spec;
use File::Path;
use XML::LibXML ':libxml';

use Baldr::ClassIndex;
use Baldr::AttributeObject;

use Baldr::ILogger;
use Baldr::DummyLogger;

use Baldr::DtdExtensions::BaldrDtdExtensions;

# Baldr instance constructor. Expects a hash with the following keys:
#   LOGGER      a Baldr::ILogger instance (optional)
sub new {
    my ($class, %attrs) = @_;
    my $self = bless {} => $class;

    # Setup logger
    $self->{log} = $attrs{LOGGER} || Baldr::DummyLogger->new;

    return $self;
}

sub _log {
    my $self = shift;
    return $self->{log}->log(@_);
}

sub _die {
    my $self = shift;
    $self->{log}->die(@_);
}

# Returns the non-namespace part of the given classname. Example: this
# function returns 'D' when given 'A::B::C::D'.
sub _nameFromClassname {
    my ($cname) = $_[0] =~ m/ ( [a-z_0-9]+ ) $ /xi;
    return $cname;
}

# Returns the non-namespace part of the given classname as a filename.
# Example: this function returns 'D.pm' when given 'A::B::C::D'.
sub _filenameFromClassname {
    my ($fname) = $_[0] =~ m/ ( [a-z_0-9]+ ) $ /xi;
    return "$fname.pm";
}

# Returns the last namespace in a given classname. Example: this function
# returns ('A', 'B', 'C') when given 'A::B::C::D'.
sub _dirnamesFromClassname {
    my @dname = $_[0] =~ m/ ( [a-z_0-9]+ ) (?= :: ) /xig;
    return @dname;
}

# Returns the name of the file corresponding to a given classname, according
# to a given path. Creates intermediary, unexistent directories if necessary.
sub _generateFilename {
    my ( $path, $classname ) = @_;
    my @nsdirs = _dirnamesFromClassname($classname);
    my $totalpath = File::Spec->catdir( $path, @nsdirs );
    File::Path::make_path($totalpath);
    return File::Spec->catfile( $totalpath, _filenameFromClassname($classname) );
}

# Arguments:
#   DTD             URL/path to the targeted DTD
#   EXTENSIONS      DTD Extensions
#   PATH            Where to generate class files
#   IMPORT_PATTERN  Pattern for files containing code to include
sub processDTD {
    my ( $self, %o ) = @_;
    my $dtd = $o{DTD};

    my $classes = Baldr::ClassIndex->new( $self->{log}, $VERSION );

    # Gets the DTD
    croak q{Can't processDTD without a DTD!} unless defined $dtd;
    $dtd = eval { XML::LibXML::Dtd->new( "-//ACU//DTD WHATEVER//EN", $dtd ) };
    if ($@) { $self->_log(qq{ When trying to build the DTD object : $@} ) }
    elsif ( !defined $dtd ) { $self->_die(qq{Failed to acquire DTD: $!}) }

    # Gets additional information
    if ( $o{EXTENSIONS} ) {
        my $p = XML::LibXML->new( no_blanks => 1, validation => 1 );
        my $fh;
        for ( ref $o{EXTENSIONS} ) {
            if ( $_ eq '' ) {
                open $fh, '<', $o{EXTENSIONS}
                  or $self->_die(qq{Can't open $o{EXTENSIONS}: $!});
            }
            elsif ( $_ eq 'IO' ) { $fh = $o{EXTENSIONS} }
            else { $self->_die(qq{Don't know what to do with $_!}) }
        }
        my $e = eval { $p->load_xml( IO => $fh ) };
        $self->_die(qq{Can't parse extension XML: $@}) if $@;
        $classes->setExtensions(
            Baldr::DtdExtensions::BaldrDtdExtensions->ParseDocumentFragment(
                $e->documentElement )    # Baldrception!
        );
    }

    # Walks the DTD
    for my $n ($dtd->childNodes) {
        next
          unless $n->nodeType == XML_ELEMENT_DECL
          || $n->nodeType == XML_ATTRIBUTE_DECL;

        $self->_log("==========================");
        $self->_log(
            $n->nodeType == XML_ELEMENT_DECL ? 'ELEMENT' : 'ATTRIBUTE',
            ' declaration: ',
            $n->nodeName, ' (of type ', ref $n, ')'
        );
        $self->_log( "\tstring representation = ", $n->toString );

        $classes->makeClassObjectFrom( $n->toString );
    }
    continue { $self->_log("\n") }

    # Stringifies all class objects
    for my $classobj ( $classes->allClassObjects ) {
        my $filename = _generateFilename( $o{PATH}, $classobj->nsName );
        my $cname = $classobj->name;

        if ( $o{IMPORT_PATTERN} ) {
            ( my $in = File::Spec->catfile( $o{PATH}, $o{IMPORT_PATTERN} ) ) =~
              s/\*/$cname/g;
            if ( -s $in ) {
                $self->_log("Using existing $in ...");
                open my $fh, '<', $in or die qq{Can't read $in: $!};
                $classobj->inserted(<$fh>);
            }
            else {
                $self->_log(
                    qq{Expansion of pattern "$o{IMPORT_PATTERN}" into "$in" }
                      . qq{didn't match any non-empty file} );
            }
        }

        open my $fh, '>', $filename or die qq{Can't write into $filename: $!};
        print {$fh} $classobj->perl;
        $self->_log(qq{Wrote $filename.});
    }

    $self->_log( qq{Processing DTD done!} );
}
