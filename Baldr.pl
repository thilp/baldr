#!/usr/bin/env perl

use 5.010_001;
use strict;
use warnings;
use autodie;

our $VERSION = "0.1";

use Getopt::Long qw(:config auto_help auto_version bundling);
use Pod::Usage;

use Baldr;
use BaldrLogger;

my ( $verbose, $path, $pattern, $extensions ) =
  ( 0, '.', '', undef );
my $logger = BaldrLogger->new;

GetOptions(
    'v|verbose!'    => \$verbose,
    'p|path=s'      => \$path,
    'i|import=s'    => \$pattern,
    'extensions=s' => \$extensions,
) or pod2usage(2);
$logger->active($verbose);
die "'$path' is not a directory\n" unless -d $path;

my $BALDR = Baldr->new( LOGGER => $logger );

$BALDR->processDTD(
    DTD            => $ARGV[0],
    EXTENSIONS     => $extensions,
    PATH           => $path,
    IMPORT_PATTERN => $pattern,
);
