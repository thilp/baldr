package Baldr::DTDObjects::Mixed;
use 5.010_001;
use strict;
use warnings;

# Mixed ::=   '(' S? '#PCDATA' (S? '|' S? Name)* S? ')*'
#           | '(' S? '#PCDATA' S? ')'

use parent 'Baldr::DTDObjects::ContentSpec';

use Carp 'croak';

sub new {
    my ( $class, @names ) = @_;
    if (my ($no) = grep { !ref || !$_->isa('Baldr::DTDObjects::Name') } @names ) {
        croak 'expected Baldr::DTDObjects::Name, got "', ref $no, '"'
    }
    return bless Baldr::DTDObjects::ContentSpec->new(
        'Mixed',
        names  => [@names],
    ) => $class;
}

sub names { @{ $_[0]->{names} } }

1;
