package Baldr::DTDObjects::DefaultDecl;
use 5.010_001;
use strict;
use warnings;

use Carp;

use constant {
    REQUIRED => 1,
    IMPLIED  => 2,
    FIXED    => 4,
    NOTHING  => 8,
};

sub new {
    my $class = shift;

    my $self = bless { type => NOTHING } => $class;

    my $type = shift or croak 'Expected at least one argument';

    if    ( $type eq '#REQUIRED' ) { $self->{type} = REQUIRED }
    elsif ( $type eq '#IMPLIED' )  { $self->{type} = IMPLIED }
    elsif ( $type eq '#FIXED' ) {
        $self->{type} = FIXED;
        $type = shift;
    }

    $type = substr($type, 1, length($type) - 2);
    $self->{value} = $type if $self->{type} & ( NOTHING | FIXED );

    return $self;
}

sub is_required { $_[0]->{type} & REQUIRED }
sub is_implied  { $_[0]->{type} & IMPLIED }
sub is_fixed    { $_[0]->{type} & FIXED }
sub is_bare     { $_[0]->{type} & NOTHING }

sub value {
    carp 'Warning: trying to get the value of '
      . 'a #REQUIRED or #IMPLIED default value'
      if $_[0]->{type} & ( REQUIRED | IMPLIED );
    return $_[0]->{value};
}

1;
