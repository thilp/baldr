package Baldr::DTDObjects::Name;
use 5.010_001;
use strict;
use warnings;

use Carp;

# Name ::= NameStartChar (NameChar)*
# NameStartChar ::= ":" | [A-Z] | "_" | [a-z] | [#xC0-#xD6] |
#               [#xD8-#xF6] | [#xF8-#x2FF] | [#x370-#x37D] | [#x37F-#x1FFF] |
#               [#x200C-#x200D] | [#x2070-#x218F] | [#x2C00-#x2FEF] | [#x3001-#xD7FF] |
#               [#xF900-#xFDCF] | [#xFDF0-#xFFFD] | [#x10000-#xEFFFF]
# NameChar ::= NameStartChar | "-" | "." | [0-9] | #xB7 | [#x0300-#x036F] | [#x203F-#x2040]

sub new {
    my ($class, $value) = @_;
        croak qq{Invalid value "value"} if defined $value and $value !~ m@
            (?&NameStartChar) (?&NameChar)*
            (?(DEFINE)
                (?<NameStartChar>
                        : | _ | [A-Za-z\x{C0}-\x{D6}\x{D8}-\x{F6}\x{F8}-\x{2FF}]
                    |   [\x{370}-\x{37D}\x{37F}-\x{1FFF}\x{200C}-\x{200D}]
                    |   [\x{2070}-\x{218F}\x{2C00}-\x{2FEF}\x{3001}-\x{D7FF}]
                    |   [\x{F900}-\x{FDCF}\x{FDF0}-\x{FFFD}\x{10000}-\x{EFFFF}]
                )
                (?<NameChar>
                        (?&NameStartChar) | - | \.
                    |   [0-9\x{B7}\x{0300}-\x{036F}\x{203F}-\x{2040}]
                )
            )
        @x;
    return bless { value => $value } => $class;
}

sub value { $_[0]->{value} }

1;
