package Baldr::DTDObjects::AttDef;
use 5.010_001;
use strict;
use warnings;

use Carp 'croak';

# AttDef ::= S Name S AttType S DefaultDecl

sub new {
    my ( $class, $name, $type, $default ) = @_;
    if ( defined $type ) {
        ref $type or croak 'Expected a Baldr::DTDObjects::AttType, got a non-ref';
        $type->isa('Baldr::DTDObjects::AttType')
          or croak 'Expected a Baldr::DTDObjects::AttType, got ', ref $type;
    }
    return
      bless { name => $name, type => $type, default => $default } => $class;
}

sub name {
    my $self = shift;
    $self->{name} = shift if @_;
    return $self->{name};
}

sub type {
    my $self = shift;
    if ( my $type = shift ) {
        ref $type or croak 'Expected a Baldr::DTDObjects::AttType, got a non-ref';
        $type->isa('Baldr::DTDObjects::AttType')
          or croak 'Expected a Baldr::DTDObjects::AttType, got ', ref $type;
        $self->{type} = $type;
    }
    return $self->{type};
}

sub default {
    my $self = shift;
    $self->{default} = shift if @_;
    return $self->{default};
}

1;
