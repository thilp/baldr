package Baldr::DTDObjects::AbstractCPAccumulator;
use 5.010_001;
use strict;
use warnings;

use Carp;

sub new {
    my $class = shift;
    return bless { list => [] } => $class;
}

sub add {
    my $self = shift;
    for my $son (grep { defined } @_) {
        croak "expecting Baldr::DTDObjects::CP, got ", ref $son
          unless $son->isa('Baldr::DTDObjects::CP');
        push @{$self->{list}}, $son;
    }
    return $self;
}

sub list { @{$_[0]->{list}} }

1;
