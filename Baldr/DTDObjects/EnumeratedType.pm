package Baldr::DTDObjects::EnumeratedType;
use 5.010_001;
use strict;
use warnings;

use parent 'Baldr::DTDObjects::AttType';

use Carp;

sub new {
    my $class = shift;
    my $self = $class->SUPER::new;
    $self->{list} = [ grep { defined } @_ ];
    return $self;
}

sub add {
    my $self = shift;
    push @{$self->{list}} => $_ for grep { defined } @_;
    return $self;
}

sub list { @{$_[0]->{list}} }

1;
