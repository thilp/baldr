package Baldr::DTDObjects::ContentSpec;
use 5.010_001;
use strict;
use warnings;

use Carp 'croak';

# contentspec ::= 'EMPTY' | 'ANY' | Mixed | children

sub new {
    my ( $class, $type, %attrs ) = @_;
    croak
      "Unexpected type '$type', expected 'EMPTY', 'ANY', 'Mixed' or 'children'"
      unless $type eq 'EMPTY'
      || $type eq 'ANY'
      || $type eq 'Mixed'
      || $type eq 'children';
    return bless { type => $type, %attrs } => $class;
}

sub type {
    my $self = shift;
    if (@_) {
        my $type = shift;
        croak "Unexpected type '$type', expected 'EMPTY', 'ANY',"
          . " 'Mixed' or 'children'"
          unless $type eq 'EMPTY'
          || $type eq 'ANY'
          || $type eq 'Mixed'
          || $type eq 'children';
        $self->{type} = $type;
    }
    return $self->{type};
}

1;
