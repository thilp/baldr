package Baldr::DTDObjects::AttlistDecl;
use 5.010_001;
use strict;
use warnings;

use Carp 'croak';

# AttlistDecl ::= '<!ATTLIST' S Name AttDef* S? '>'

sub new {
    my ( $class, $name, @attdefs ) = @_;
    return bless {
        name    => $name,
        attdefs => [@attdefs],
    } => $class;
}

sub name {
    my $self = shift;
    $self->{name} = shift if @_;
    return $self->{name};
}

sub add {
    my $self = shift;
    for my $attdef ( grep { defined } @_ ) {
        croak "expecting Baldr::DTDObjects::AttDef, got ", ref $attdef
          unless $attdef->isa('Baldr::DTDObjects::AttDef');
        push @{ $self->{attdefs} }, $attdef;
    }
    return $self;
}

sub list { @{ $_[0]->{attdefs} } }

1;
