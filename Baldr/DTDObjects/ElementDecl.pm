package Baldr::DTDObjects::ElementDecl;
use 5.010_001;
use strict;
use warnings;

use Carp 'croak';

# elementdecl ::= '<!ELEMENT' S Name S contentspec S? '>'

sub new {
    my ( $class, $name, $contentspec ) = @_;
    return bless {
        name        => $name,
        contentspec => $contentspec,
    } => $class;
}

sub name {
    my $self = shift;
    $self->{name} = shift if @_;
    return $self->{name};
}

sub contentspec {
    my $self = shift;
    if (@_) {
        my $cs = shift;
        croak 'Expected Baldr::DTDObjects::ContentSpec, got ', ref $cs
          unless ref $cs and $cs->isa('Baldr::DTDObjects::ContentSpec');
        $self->{contentspec} = $cs;
    }
    return $self->{contentspec};
}

1;
