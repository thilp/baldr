package Baldr::DTDObjects::TokenizedType;
use 5.010_001;
use strict;
use warnings;

use parent 'Baldr::DTDObjects::AttType';

use Carp 'croak';

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new;
    $self->{type} = do {
        my $type = shift;
        croak 'Expected ID or IDREF or IDREFS'
          if !defined $type || !grep { $type eq $_ } qw( ID IDREF IDREFS );
        $type;
    };
    return $self;
}

sub type { $_[0]->{type} }

1;
