package Baldr::DTDObjects::CP;    # Content Particles
use 5.010_001;
use strict;
use warnings;

# cp ::= (Name | choice | seq) ('?' | '*' | '+')?

use Carp;

sub new {
    my ( $class, $multiplicity, $value ) = @_;
    $multiplicity ||= '1';
    croak "invalid multiplicity '$multiplicity'"
      unless grep { $multiplicity eq $_ } qw( + * ? 1 );
    if ($value) {
        croak 'expected Baldr::DTDObjects::Choice or Baldr::DTDObjects::Seq '
          . 'or Baldr::DTDObjects::Name, got a non-ref'
          if ref $value eq '';
        croak 'expected Baldr::DTDObjects::Choice or Baldr::DTDObjects::Seq '
          . 'or Baldr::DTDObjects::Name, got ', ref $value
          if !$value->isa('Baldr::DTDObjects::Choice')
          and !$value->isa('Baldr::DTDObjects::Seq')
          and !$value->isa('Baldr::DTDObjects::Name');
    }
    return bless { multiplicity => $multiplicity, value => $value } => $class;
}

sub multiplicity {
    my $self = shift;
    if (@_) {
        croak "invalid multiplicity '$_[0]'"
          unless grep { $_[0] eq $_ } qw( + * ? 1 );
        $self->{multiplicity} = $_[0];
    }
    return $self->{multiplicity};
}

sub value {
    my $self = shift;
    if (@_) {
        croak 'expected Baldr::DTDObjects::Choice or Baldr::DTDObjects::Seq '
          . 'or Baldr::DTDObjects::Name, got a non-ref'
          if !defined $_[0]
          or ref $_[0] eq '';
        croak 'expected Baldr::DTDObjects::Choice or Baldr::DTDObjects::Seq '
          . 'or Baldr::DTDObjects::Name, got ', ref $_[0]
          if !$_[0]->isa('Baldr::DTDObjects::Choice')
          and !$_[0]->isa('Baldr::DTDObjects::Seq')
          and !$_[0]->isa('Baldr::DTDObjects::Name');
        $self->{value} = $_[0];
    }
    return $self->{value};
}

1;
