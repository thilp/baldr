package Baldr::ILogger;

use 5.010_001;
use strict;
use warnings;
use autodie;

# Constructor
sub new {
    my ( $class, $out ) = @_;
    return bless {
        active => 1,                         # Does this logger currently log?
        output => $out || *STDOUT,           # Targeted log output
        isatty => -t ( $out || *STDOUT ),    # Is 'output' a TTY?
    } => $class;
}

# Getter/setter: 1 if this logger is active
sub active {
    my $self = shift;
    $self->{active} = shift if @_;
    return $self->{active};
}

# Setter for output stream (default: STDOUT).
sub setOutput {
    $_[0]->{output} = shift;
}

# Getter to know if the logger runs in a TTY.
sub getIsatty { $_[0]->{isatty} }

# Logging method. Given a string, does whatever this logger is for with it.
sub log { die 'Override me!' }

# Error method. Given a string, does whatever this logger is for with it and
# then dies.
sub die { die 'Override me!' }

1;
