package Baldr::AttributeObjectType;
use 5.010_001;
use strict;
use warnings;

use Carp 'croak';

sub new {
    my ( $class, %args ) = @_;
    return bless \%args => $class;
}

sub str { '{' . ref( $_[0] ) . '}' }

sub what { undef }

1;
