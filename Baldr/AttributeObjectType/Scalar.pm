package Baldr::AttributeObjectType::Scalar;
use 5.010_001;
use strict;
use warnings;

use parent 'Baldr::AttributeObjectType';

sub new {
    my $class = shift;
    return $class->SUPER::new;
}

sub str { '{' . ref( $_[0] ) . '}' }

sub what { 'scalar' }

1;
