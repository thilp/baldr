package Baldr::AttributeObjectType::Class;
use 5.010_001;
use strict;
use warnings;

use parent 'Baldr::AttributeObjectType';

sub new {
    my $class = shift;
    return $class->SUPER::new( name => shift );
}

sub name { $_[0]->{name} }

sub str { '{' . ref( $_[0] ) . '|' . $_[0]->name . '}' }

sub what { 'class' }

1;
