package Baldr::AttributeObjectType::Enum;
use 5.010_001;
use strict;
use warnings;

use parent 'Baldr::AttributeObjectType';

sub new {
    my $class = shift;
    return $class->SUPER::new( values => [ @_ ] );
}

sub values { @{ $_[0]->{values} } }

sub str {
    '{' . ref( $_[0] ) . '|' . join( '/', $_[0]->values ) . '}';
}

sub what { 'enum' }

1;
