package Baldr::DummyLogger;

use 5.010_001;
use strict;
use warnings;
use autodie;

use parent 'Baldr::ILogger';

use Carp;

# Does nothing!
sub log { 1 }

sub die { shift; confess @_ }

1;
