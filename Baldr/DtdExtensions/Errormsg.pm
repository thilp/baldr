# Class automatically generated from a DTD by Baldr
# <https://bitbucket.org/thilp/baldr>
# on Sun Feb  9 05:13:29 2014

package Baldr::DtdExtensions::Errormsg;
use 5.010_001;
use strict;
use warnings;

use Carp;
use Scalar::Util 'looks_like_number', 'blessed';




sub new {
    my ($class, %args) = @_;
    return our @ISA ? $class->SUPER::new(%args) : bless \%args => $class;
}


# Not used, because no assertions have been registered for this class.
# These calls will be optimized out by Perl.
sub _check_assertions { 1 };
sub _suspendAssertions { 1 };
sub _reactivateAssertions { 1 };


# Simple getter for "value".
sub Getvalue { return $_[0]->{_value} //= undef }

# Simple setter for "value". No type checking on the argument.
# Returns the updated object.
sub Setvalue {
    $_[0]->{_value} = $_[1];
    $_[0]->_check_assertions;
    return $_[0];
}

# Remove all blanks before and after the attribute value and returns it.
# (The attribute itself is affected, not only this method's return value.)
sub Chompvalue {
    ($_[0]->{_value} //= undef) =~ s/ \A \s+ | \s+ \z //xg;
    $_[0]->_check_assertions;
    $_[0]->{_value}
}






# Helper for the next function.
sub _attrname {
    my ($name) = @_;
    $name =~ s/ [_-]+ ([a-z]) /uc($1)/iegx;
    return ucfirst $name;
}

# ParseInvalidNodeHook allows user-defined behavior when a XML node given as
# argument to ParseDocumentFragment does not correspond to the caller class.
# This method sets ParseInvalidNodeHook to the given value.
sub SetInvalidNodeHook { our $ParseInvalidNodeHook = $_[1]; return }

# ParseUnknownClassHook allows user-defined behavior when the class
# corresponding to the node does not exist or does not implement
# ParseDocumentFragment.
# This method sets ParseUnknownClassHook to the given value.
sub SetUnknownClassHook { our $ParseUnknownClassHook = $_[1]; return }

# Autoparser for Baldr::DtdExtensions::Errormsg objects.
# Constructs a Baldr::DtdExtensions::Errormsg from the given XML element. If the string uses nodes
# with no known classes attached to them, user-defined hooks are run.
# See SetUnknownClassHook and SetInvalidNodeHook.
sub ParseDocumentFragment {
    my ($self, $node) = @_;
    our $ParseUnknownClassHook //= sub {
        confess 'UnknownClassHook not defined for node "', $_[0]->nodeName, '"';
    };
    our $ParseInvalidNodeHook //= sub {
        confess 'ParseInvalidNodeHook not defined for node "', $_[0]->nodeName, '"';
    };
    my $classname = _attrname($node->nodeName);
    $ParseInvalidNodeHook->($node) unless $classname eq 'Errormsg';

    my $newObj = Baldr::DtdExtensions::Errormsg->new;
    $newObj->_suspendAssertions; # object construction

    $newObj->Setvalue($_->textContent);
    $newObj->_reactivateAssertions; # construction done, check assertions now
    return $newObj;
}


1;
