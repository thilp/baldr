# Class automatically generated from a DTD by Baldr
# <https://bitbucket.org/thilp/baldr>
# on Sun Feb  9 05:13:29 2014

package Baldr::DtdExtensions::Coercion;
use 5.010_001;
use strict;
use warnings;

use Carp;
use Scalar::Util 'looks_like_number', 'blessed';




sub new {
    my ($class, %args) = @_;
    return our @ISA ? $class->SUPER::new(%args) : bless \%args => $class;
}


# Not used, because no assertions have been registered for this class.
# These calls will be optimized out by Perl.
sub _check_assertions { 1 };
sub _suspendAssertions { 1 };
sub _reactivateAssertions { 1 };


# Simple getter for "Baldr::DtdExtensions::To".
sub GetBaldrDtdExtensionsTo { return $_[0]->{_BaldrDtdExtensionsTo} //= undef }

# Simple setter for "Baldr::DtdExtensions::To". No type checking on the argument.
# Returns the updated object.
sub SetBaldrDtdExtensionsTo {
    $_[0]->{_BaldrDtdExtensionsTo} = $_[1];
    $_[0]->_check_assertions;
    return $_[0];
}

# Remove all blanks before and after the attribute value and returns it.
# (The attribute itself is affected, not only this method's return value.)
sub ChompBaldrDtdExtensionsTo {
    ($_[0]->{_BaldrDtdExtensionsTo} //= undef) =~ s/ \A \s+ | \s+ \z //xg;
    $_[0]->_check_assertions;
    $_[0]->{_BaldrDtdExtensionsTo}
}


# List getter for "Baldr::DtdExtensions::In".
# With no argument, returns all the elements of the list.
# With an argument, and if this argument is a number, returns an element of
# the list by its index (works as expected with negative indexes).
sub GetBaldrDtdExtensionsIn {
    if (defined $_[1]) {
        croak 'Expecting a numeric index, got "', $_[1], '"'
          unless looks_like_number($_[1]);
        return $_[0]->{_name}->[$_[1]] //= undef;
    }
    else {
        return @{ $_[0]->{_BaldrDtdExtensionsIn} };
    }
}

# ArrayRef getter for "Baldr::DtdExtensions::In".
# Returns an array reference on a (shallow) copy of the list.
sub GetBaldrDtdExtensionsInRef { my @list = @{ $_[0]->{_BaldrDtdExtensionsIn} }; return \@list }

# Empties the current list for "Baldr::DtdExtensions::In".
# Returns the updated object.
sub ForgetBaldrDtdExtensionsIn {
    $_[0]->{_BaldrDtdExtensionsIn} = [];
    $_[0]->_check_assertions;
    return $_[0];
}

# Emptiness test.
# Returns 0 if the list is empty, 1 otherwise.
sub HasBaldrDtdExtensionsIn { scalar @{ $_[0]->{_BaldrDtdExtensionsIn} } ? 1 : 0 }

# Counter.
# Returns the number of elements of the list.
sub CountBaldrDtdExtensionsIn { scalar @{ $_[0]->{_BaldrDtdExtensionsIn} } }

# Pop.
# See &CORE::pop.
sub PopBaldrDtdExtensionsIn {
    my $ret = CORE::pop( @{ $_[0]->{_BaldrDtdExtensionsIn} } );
    $_[0]->_check_assertions;
    return $ret;
}

# Shift.
# See &CORE::shift.
sub ShiftBaldrDtdExtensionsIn {
    my $ret = CORE::shift( @{ $_[0]->{_BaldrDtdExtensionsIn} } );
    $_[0]->_check_assertions;
    return $ret;
}

# List setter for "Baldr::DtdExtensions::In".
# Clears the attribute's content, then uses the arguments as new elements of
# the list. Returns the updated object.
# Dies if one of the arguments is not a subtype of "Baldr::DtdExtensions::In".
sub SetBaldrDtdExtensionsIn {
    my $self = shift;
    if ( local ( $_ ) = grep { !blessed $_ || !$_->isa('Baldr::DtdExtensions::In') } @_ ) {
        if ( not defined ) {
            croak 'Expecting Baldr::DtdExtensions::In, got undef '
              . '(you might want to use "ForgetBaldrDtdExtensionsIn" instead)';
        }
        elsif ( not ref ) {
            croak "Expecting Baldr::DtdExtensions::In, got a non-ref \"$_\"";
        }
        elsif ( not blessed $_ ) {
            croak 'Expecting Baldr::DtdExtensions::In, got an unblessed ref ', ref;
        }
        else {    # not $_->isa('Baldr::DtdExtensions::In')
            croak 'Expecting Baldr::DtdExtensions::In, got ', ref;
        }
    }
    $self->{_BaldrDtdExtensionsIn} = [ @_ ];
    $self->_check_assertions;
    return $self;
}

# ArrayRef setter for "Baldr::DtdExtensions::In"
# Replaces the attribute's value with the union of the arguments.
# Dies if one of the arguments is not an arrayref, or if an arrayref contains
# any element that is not of a subtype of "Baldr::DtdExtensions::In".
# Returns the updated object.
sub SetBaldrDtdExtensionsInRef {
    my $self = shift;
    my @final;
    for my $a ( @_ ) {
        if ( !ref $a ) {
            if ( not defined $a ) {
                croak 'Expecting an arrayref, got undef '
                  . '(you might want to use "ForgetBaldrDtdExtensionsIn" instead)';
            }
            else { croak 'Expecting an arrayref, got "', $a, '"' }
        }
        elsif ( local ($_) = grep { !blessed $_ || !$_->isa('Baldr::DtdExtensions::In') } @$a ) {
            if ( not defined ) {
                croak 'Expecting Baldr::DtdExtensions::In, got undef '
                  . '(you might want to use "ForgetBaldrDtdExtensionsIn" instead)';
            }
            elsif ( not ref ) {
                croak "Expecting Baldr::DtdExtensions::In, got a non-ref \"$_\"";
            }
            elsif ( not blessed $_ ) {
                croak 'Expecting Baldr::DtdExtensions::In, got an unblessed ref ', ref;
            }
            else {    # not $_->isa('Baldr::DtdExtensions::In')
                croak 'Expecting Baldr::DtdExtensions::In, got ', ref;
            }
        }
        else { push @final, @$a }
    }

    $self->{_BaldrDtdExtensionsIn} = \@final;
    $self->_check_assertions;

    return $self;
}

# Push
# See &CORE::push. Dies if any argument is not of a subtype of "Baldr::DtdExtensions::In".
sub PushBaldrDtdExtensionsIn {
    my $self = shift;
    if ( local ( $_ ) = grep { !blessed $_ || !$_->isa('Baldr::DtdExtensions::In') } @_ ) {
        if ( not defined ) {
            croak 'Expecting Baldr::DtdExtensions::In, got undef '
              . '(you might want to use "ForgetBaldrDtdExtensionsIn" instead)';
        }
        elsif ( not ref ) {
            croak "Expecting Baldr::DtdExtensions::In, got a non-ref \"$_\"";
        }
        elsif ( not blessed $_ ) {
            croak 'Expecting Baldr::DtdExtensions::In, got an unblessed ref ', ref;
        }
        else {    # not $_->isa('Baldr::DtdExtensions::In')
            croak 'Expecting Baldr::DtdExtensions::In, got ', ref;
        }
    }
    my $ret = CORE::push( @{ $self->{_BaldrDtdExtensionsIn} }, @_ );
    $self->_check_assertions;
    return $ret;
}

# Unshift
# See &CORE::unshift. Dies if any argument is not of a subtype of "Baldr::DtdExtensions::In".
sub UnshiftBaldrDtdExtensionsIn {
    my $self = shift;
    if ( local ( $_ ) = grep { !blessed $_ || !$_->isa('Baldr::DtdExtensions::In') } @_ ) {
        if ( not defined ) {
            croak 'Expecting Baldr::DtdExtensions::In, got undef '
              . '(you might want to use "ForgetBaldrDtdExtensionsIn" instead)';
        }
        elsif ( not ref ) {
            croak "Expecting Baldr::DtdExtensions::In, got a non-ref \"$_\"";
        }
        elsif ( not blessed $_ ) {
            croak 'Expecting Baldr::DtdExtensions::In, got an unblessed ref ', ref;
        }
        else {    # not $_->isa('Baldr::DtdExtensions::In')
            croak 'Expecting Baldr::DtdExtensions::In, got ', ref;
        }
    }
    my $ret = CORE::unshift( @{ $self->{_BaldrDtdExtensionsIn} }, @_ );
    $self->_check_assertions;
    return $ret;
}




# Helper for the next function.
sub _attrname {
    my ($name) = @_;
    $name =~ s/ [_-]+ ([a-z]) /uc($1)/iegx;
    return ucfirst $name;
}

# ParseInvalidNodeHook allows user-defined behavior when a XML node given as
# argument to ParseDocumentFragment does not correspond to the caller class.
# This method sets ParseInvalidNodeHook to the given value.
sub SetInvalidNodeHook { our $ParseInvalidNodeHook = $_[1]; return }

# ParseUnknownClassHook allows user-defined behavior when the class
# corresponding to the node does not exist or does not implement
# ParseDocumentFragment.
# This method sets ParseUnknownClassHook to the given value.
sub SetUnknownClassHook { our $ParseUnknownClassHook = $_[1]; return }

# Autoparser for Baldr::DtdExtensions::Coercion objects.
# Constructs a Baldr::DtdExtensions::Coercion from the given XML element. If the string uses nodes
# with no known classes attached to them, user-defined hooks are run.
# See SetUnknownClassHook and SetInvalidNodeHook.
sub ParseDocumentFragment {
    my ($self, $node) = @_;
    our $ParseUnknownClassHook //= sub {
        confess 'UnknownClassHook not defined for node "', $_[0]->nodeName, '"';
    };
    our $ParseInvalidNodeHook //= sub {
        confess 'ParseInvalidNodeHook not defined for node "', $_[0]->nodeName, '"';
    };
    my $classname = _attrname($node->nodeName);
    $ParseInvalidNodeHook->($node) unless $classname eq 'Coercion';

    my $newObj = Baldr::DtdExtensions::Coercion->new;
    $newObj->_suspendAssertions; # object construction

    foreach ($node->attributes) {
        my $changed_name = _attrname($_->nodeName);
        if (0) { die } # to have elsifs only
        elsif ($changed_name eq 'To') { $newObj->SetBaldrDtdExtensionsTo($_->textContent); }
    }
    foreach ($node->nonBlankChildNodes) {
        my $changed_name = _attrname($_->nodeName);
        if (0) { die } # to have elsifs only
        elsif ($changed_name eq 'In') {
            my $o = eval { require Baldr::DtdExtensions::In && Baldr::DtdExtensions::In->can('ParseDocumentFragment') }
                   ? Baldr::DtdExtensions::In->ParseDocumentFragment($_)
                   : $ParseUnknownClassHook->($_);
            $newObj->PushBaldrDtdExtensionsIn($o);
        }
    }
    $newObj->_reactivateAssertions; # construction done, check assertions now
    return $newObj;
}


1;
