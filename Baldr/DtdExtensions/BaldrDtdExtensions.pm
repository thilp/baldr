# Class automatically generated from a DTD by Baldr
# <https://bitbucket.org/thilp/baldr>
# on Sun Feb  9 05:13:29 2014

package Baldr::DtdExtensions::BaldrDtdExtensions;
use 5.010_001;
use strict;
use warnings;

use Carp;
use Scalar::Util 'looks_like_number', 'blessed';




sub new {
    my ($class, %args) = @_;
    return our @ISA ? $class->SUPER::new(%args) : bless \%args => $class;
}


# Not used, because no assertions have been registered for this class.
# These calls will be optimized out by Perl.
sub _check_assertions { 1 };
sub _suspendAssertions { 1 };
sub _reactivateAssertions { 1 };


# Simple getter for "Baldr::DtdExtensions::Namespace".
sub GetBaldrDtdExtensionsNamespace { return $_[0]->{_BaldrDtdExtensionsNamespace} //= undef }

# Simple setter for "Baldr::DtdExtensions::Namespace".
# Returns the updated object.
# Dies if the argument is not a subtype of "Baldr::DtdExtensions::Namespace".
sub SetBaldrDtdExtensionsNamespace {
    croak 'Expecting Baldr::DtdExtensions::Namespace, got undef '
      . '(you might want to use "ForgetBaldrDtdExtensionsNamespace" instead)'
      unless defined $_[1];

    if (!blessed $_[1]) {
        if (ref $_[1]) {
            croak 'Expecting Baldr::DtdExtensions::Namespace, got an unblessed ', ref $_[1];
        }
        else { croak "Expecting Baldr::DtdExtensions::Namespace, got a non-ref \"$_[1]\"" }
    }
    croak 'Expecting Baldr::DtdExtensions::Namespace, got ', ref $_[1] unless $_[1]->isa('Baldr::DtdExtensions::Namespace');

    $_[0]->{_BaldrDtdExtensionsNamespace} = $_[1];
    $_[0]->_check_assertions;

    return $_[0];
}

# Erases the current value for "Baldr::DtdExtensions::Namespace" and replaces it with undef, as if no
# previous values had been set.
# Returns the updated object.
sub ForgetBaldrDtdExtensionsNamespace {
    $_[0]->{_BaldrDtdExtensionsNamespace} = undef;
    $_[0]->_check_assertions;
    return $_[0];
}


# List getter for "Baldr::DtdExtensions::Coercion".
# With no argument, returns all the elements of the list.
# With an argument, and if this argument is a number, returns an element of
# the list by its index (works as expected with negative indexes).
sub GetBaldrDtdExtensionsCoercion {
    if (defined $_[1]) {
        croak 'Expecting a numeric index, got "', $_[1], '"'
          unless looks_like_number($_[1]);
        return $_[0]->{_name}->[$_[1]] //= undef;
    }
    else {
        return @{ $_[0]->{_BaldrDtdExtensionsCoercion} };
    }
}

# ArrayRef getter for "Baldr::DtdExtensions::Coercion".
# Returns an array reference on a (shallow) copy of the list.
sub GetBaldrDtdExtensionsCoercionRef { my @list = @{ $_[0]->{_BaldrDtdExtensionsCoercion} }; return \@list }

# Empties the current list for "Baldr::DtdExtensions::Coercion".
# Returns the updated object.
sub ForgetBaldrDtdExtensionsCoercion {
    $_[0]->{_BaldrDtdExtensionsCoercion} = [];
    $_[0]->_check_assertions;
    return $_[0];
}

# Emptiness test.
# Returns 0 if the list is empty, 1 otherwise.
sub HasBaldrDtdExtensionsCoercion { scalar @{ $_[0]->{_BaldrDtdExtensionsCoercion} } ? 1 : 0 }

# Counter.
# Returns the number of elements of the list.
sub CountBaldrDtdExtensionsCoercion { scalar @{ $_[0]->{_BaldrDtdExtensionsCoercion} } }

# Pop.
# See &CORE::pop.
sub PopBaldrDtdExtensionsCoercion {
    my $ret = CORE::pop( @{ $_[0]->{_BaldrDtdExtensionsCoercion} } );
    $_[0]->_check_assertions;
    return $ret;
}

# Shift.
# See &CORE::shift.
sub ShiftBaldrDtdExtensionsCoercion {
    my $ret = CORE::shift( @{ $_[0]->{_BaldrDtdExtensionsCoercion} } );
    $_[0]->_check_assertions;
    return $ret;
}

# List setter for "Baldr::DtdExtensions::Coercion".
# Clears the attribute's content, then uses the arguments as new elements of
# the list. Returns the updated object.
# Dies if one of the arguments is not a subtype of "Baldr::DtdExtensions::Coercion".
sub SetBaldrDtdExtensionsCoercion {
    my $self = shift;
    if ( local ( $_ ) = grep { !blessed $_ || !$_->isa('Baldr::DtdExtensions::Coercion') } @_ ) {
        if ( not defined ) {
            croak 'Expecting Baldr::DtdExtensions::Coercion, got undef '
              . '(you might want to use "ForgetBaldrDtdExtensionsCoercion" instead)';
        }
        elsif ( not ref ) {
            croak "Expecting Baldr::DtdExtensions::Coercion, got a non-ref \"$_\"";
        }
        elsif ( not blessed $_ ) {
            croak 'Expecting Baldr::DtdExtensions::Coercion, got an unblessed ref ', ref;
        }
        else {    # not $_->isa('Baldr::DtdExtensions::Coercion')
            croak 'Expecting Baldr::DtdExtensions::Coercion, got ', ref;
        }
    }
    $self->{_BaldrDtdExtensionsCoercion} = [ @_ ];
    $self->_check_assertions;
    return $self;
}

# ArrayRef setter for "Baldr::DtdExtensions::Coercion"
# Replaces the attribute's value with the union of the arguments.
# Dies if one of the arguments is not an arrayref, or if an arrayref contains
# any element that is not of a subtype of "Baldr::DtdExtensions::Coercion".
# Returns the updated object.
sub SetBaldrDtdExtensionsCoercionRef {
    my $self = shift;
    my @final;
    for my $a ( @_ ) {
        if ( !ref $a ) {
            if ( not defined $a ) {
                croak 'Expecting an arrayref, got undef '
                  . '(you might want to use "ForgetBaldrDtdExtensionsCoercion" instead)';
            }
            else { croak 'Expecting an arrayref, got "', $a, '"' }
        }
        elsif ( local ($_) = grep { !blessed $_ || !$_->isa('Baldr::DtdExtensions::Coercion') } @$a ) {
            if ( not defined ) {
                croak 'Expecting Baldr::DtdExtensions::Coercion, got undef '
                  . '(you might want to use "ForgetBaldrDtdExtensionsCoercion" instead)';
            }
            elsif ( not ref ) {
                croak "Expecting Baldr::DtdExtensions::Coercion, got a non-ref \"$_\"";
            }
            elsif ( not blessed $_ ) {
                croak 'Expecting Baldr::DtdExtensions::Coercion, got an unblessed ref ', ref;
            }
            else {    # not $_->isa('Baldr::DtdExtensions::Coercion')
                croak 'Expecting Baldr::DtdExtensions::Coercion, got ', ref;
            }
        }
        else { push @final, @$a }
    }

    $self->{_BaldrDtdExtensionsCoercion} = \@final;
    $self->_check_assertions;

    return $self;
}

# Push
# See &CORE::push. Dies if any argument is not of a subtype of "Baldr::DtdExtensions::Coercion".
sub PushBaldrDtdExtensionsCoercion {
    my $self = shift;
    if ( local ( $_ ) = grep { !blessed $_ || !$_->isa('Baldr::DtdExtensions::Coercion') } @_ ) {
        if ( not defined ) {
            croak 'Expecting Baldr::DtdExtensions::Coercion, got undef '
              . '(you might want to use "ForgetBaldrDtdExtensionsCoercion" instead)';
        }
        elsif ( not ref ) {
            croak "Expecting Baldr::DtdExtensions::Coercion, got a non-ref \"$_\"";
        }
        elsif ( not blessed $_ ) {
            croak 'Expecting Baldr::DtdExtensions::Coercion, got an unblessed ref ', ref;
        }
        else {    # not $_->isa('Baldr::DtdExtensions::Coercion')
            croak 'Expecting Baldr::DtdExtensions::Coercion, got ', ref;
        }
    }
    my $ret = CORE::push( @{ $self->{_BaldrDtdExtensionsCoercion} }, @_ );
    $self->_check_assertions;
    return $ret;
}

# Unshift
# See &CORE::unshift. Dies if any argument is not of a subtype of "Baldr::DtdExtensions::Coercion".
sub UnshiftBaldrDtdExtensionsCoercion {
    my $self = shift;
    if ( local ( $_ ) = grep { !blessed $_ || !$_->isa('Baldr::DtdExtensions::Coercion') } @_ ) {
        if ( not defined ) {
            croak 'Expecting Baldr::DtdExtensions::Coercion, got undef '
              . '(you might want to use "ForgetBaldrDtdExtensionsCoercion" instead)';
        }
        elsif ( not ref ) {
            croak "Expecting Baldr::DtdExtensions::Coercion, got a non-ref \"$_\"";
        }
        elsif ( not blessed $_ ) {
            croak 'Expecting Baldr::DtdExtensions::Coercion, got an unblessed ref ', ref;
        }
        else {    # not $_->isa('Baldr::DtdExtensions::Coercion')
            croak 'Expecting Baldr::DtdExtensions::Coercion, got ', ref;
        }
    }
    my $ret = CORE::unshift( @{ $self->{_BaldrDtdExtensionsCoercion} }, @_ );
    $self->_check_assertions;
    return $ret;
}
# List getter for "Baldr::DtdExtensions::Constraint".
# With no argument, returns all the elements of the list.
# With an argument, and if this argument is a number, returns an element of
# the list by its index (works as expected with negative indexes).
sub GetBaldrDtdExtensionsConstraint {
    if (defined $_[1]) {
        croak 'Expecting a numeric index, got "', $_[1], '"'
          unless looks_like_number($_[1]);
        return $_[0]->{_name}->[$_[1]] //= undef;
    }
    else {
        return @{ $_[0]->{_BaldrDtdExtensionsConstraint} };
    }
}

# ArrayRef getter for "Baldr::DtdExtensions::Constraint".
# Returns an array reference on a (shallow) copy of the list.
sub GetBaldrDtdExtensionsConstraintRef { my @list = @{ $_[0]->{_BaldrDtdExtensionsConstraint} }; return \@list }

# Empties the current list for "Baldr::DtdExtensions::Constraint".
# Returns the updated object.
sub ForgetBaldrDtdExtensionsConstraint {
    $_[0]->{_BaldrDtdExtensionsConstraint} = [];
    $_[0]->_check_assertions;
    return $_[0];
}

# Emptiness test.
# Returns 0 if the list is empty, 1 otherwise.
sub HasBaldrDtdExtensionsConstraint { scalar @{ $_[0]->{_BaldrDtdExtensionsConstraint} } ? 1 : 0 }

# Counter.
# Returns the number of elements of the list.
sub CountBaldrDtdExtensionsConstraint { scalar @{ $_[0]->{_BaldrDtdExtensionsConstraint} } }

# Pop.
# See &CORE::pop.
sub PopBaldrDtdExtensionsConstraint {
    my $ret = CORE::pop( @{ $_[0]->{_BaldrDtdExtensionsConstraint} } );
    $_[0]->_check_assertions;
    return $ret;
}

# Shift.
# See &CORE::shift.
sub ShiftBaldrDtdExtensionsConstraint {
    my $ret = CORE::shift( @{ $_[0]->{_BaldrDtdExtensionsConstraint} } );
    $_[0]->_check_assertions;
    return $ret;
}

# List setter for "Baldr::DtdExtensions::Constraint".
# Clears the attribute's content, then uses the arguments as new elements of
# the list. Returns the updated object.
# Dies if one of the arguments is not a subtype of "Baldr::DtdExtensions::Constraint".
sub SetBaldrDtdExtensionsConstraint {
    my $self = shift;
    if ( local ( $_ ) = grep { !blessed $_ || !$_->isa('Baldr::DtdExtensions::Constraint') } @_ ) {
        if ( not defined ) {
            croak 'Expecting Baldr::DtdExtensions::Constraint, got undef '
              . '(you might want to use "ForgetBaldrDtdExtensionsConstraint" instead)';
        }
        elsif ( not ref ) {
            croak "Expecting Baldr::DtdExtensions::Constraint, got a non-ref \"$_\"";
        }
        elsif ( not blessed $_ ) {
            croak 'Expecting Baldr::DtdExtensions::Constraint, got an unblessed ref ', ref;
        }
        else {    # not $_->isa('Baldr::DtdExtensions::Constraint')
            croak 'Expecting Baldr::DtdExtensions::Constraint, got ', ref;
        }
    }
    $self->{_BaldrDtdExtensionsConstraint} = [ @_ ];
    $self->_check_assertions;
    return $self;
}

# ArrayRef setter for "Baldr::DtdExtensions::Constraint"
# Replaces the attribute's value with the union of the arguments.
# Dies if one of the arguments is not an arrayref, or if an arrayref contains
# any element that is not of a subtype of "Baldr::DtdExtensions::Constraint".
# Returns the updated object.
sub SetBaldrDtdExtensionsConstraintRef {
    my $self = shift;
    my @final;
    for my $a ( @_ ) {
        if ( !ref $a ) {
            if ( not defined $a ) {
                croak 'Expecting an arrayref, got undef '
                  . '(you might want to use "ForgetBaldrDtdExtensionsConstraint" instead)';
            }
            else { croak 'Expecting an arrayref, got "', $a, '"' }
        }
        elsif ( local ($_) = grep { !blessed $_ || !$_->isa('Baldr::DtdExtensions::Constraint') } @$a ) {
            if ( not defined ) {
                croak 'Expecting Baldr::DtdExtensions::Constraint, got undef '
                  . '(you might want to use "ForgetBaldrDtdExtensionsConstraint" instead)';
            }
            elsif ( not ref ) {
                croak "Expecting Baldr::DtdExtensions::Constraint, got a non-ref \"$_\"";
            }
            elsif ( not blessed $_ ) {
                croak 'Expecting Baldr::DtdExtensions::Constraint, got an unblessed ref ', ref;
            }
            else {    # not $_->isa('Baldr::DtdExtensions::Constraint')
                croak 'Expecting Baldr::DtdExtensions::Constraint, got ', ref;
            }
        }
        else { push @final, @$a }
    }

    $self->{_BaldrDtdExtensionsConstraint} = \@final;
    $self->_check_assertions;

    return $self;
}

# Push
# See &CORE::push. Dies if any argument is not of a subtype of "Baldr::DtdExtensions::Constraint".
sub PushBaldrDtdExtensionsConstraint {
    my $self = shift;
    if ( local ( $_ ) = grep { !blessed $_ || !$_->isa('Baldr::DtdExtensions::Constraint') } @_ ) {
        if ( not defined ) {
            croak 'Expecting Baldr::DtdExtensions::Constraint, got undef '
              . '(you might want to use "ForgetBaldrDtdExtensionsConstraint" instead)';
        }
        elsif ( not ref ) {
            croak "Expecting Baldr::DtdExtensions::Constraint, got a non-ref \"$_\"";
        }
        elsif ( not blessed $_ ) {
            croak 'Expecting Baldr::DtdExtensions::Constraint, got an unblessed ref ', ref;
        }
        else {    # not $_->isa('Baldr::DtdExtensions::Constraint')
            croak 'Expecting Baldr::DtdExtensions::Constraint, got ', ref;
        }
    }
    my $ret = CORE::push( @{ $self->{_BaldrDtdExtensionsConstraint} }, @_ );
    $self->_check_assertions;
    return $ret;
}

# Unshift
# See &CORE::unshift. Dies if any argument is not of a subtype of "Baldr::DtdExtensions::Constraint".
sub UnshiftBaldrDtdExtensionsConstraint {
    my $self = shift;
    if ( local ( $_ ) = grep { !blessed $_ || !$_->isa('Baldr::DtdExtensions::Constraint') } @_ ) {
        if ( not defined ) {
            croak 'Expecting Baldr::DtdExtensions::Constraint, got undef '
              . '(you might want to use "ForgetBaldrDtdExtensionsConstraint" instead)';
        }
        elsif ( not ref ) {
            croak "Expecting Baldr::DtdExtensions::Constraint, got a non-ref \"$_\"";
        }
        elsif ( not blessed $_ ) {
            croak 'Expecting Baldr::DtdExtensions::Constraint, got an unblessed ref ', ref;
        }
        else {    # not $_->isa('Baldr::DtdExtensions::Constraint')
            croak 'Expecting Baldr::DtdExtensions::Constraint, got ', ref;
        }
    }
    my $ret = CORE::unshift( @{ $self->{_BaldrDtdExtensionsConstraint} }, @_ );
    $self->_check_assertions;
    return $ret;
}




# Helper for the next function.
sub _attrname {
    my ($name) = @_;
    $name =~ s/ [_-]+ ([a-z]) /uc($1)/iegx;
    return ucfirst $name;
}

# ParseInvalidNodeHook allows user-defined behavior when a XML node given as
# argument to ParseDocumentFragment does not correspond to the caller class.
# This method sets ParseInvalidNodeHook to the given value.
sub SetInvalidNodeHook { our $ParseInvalidNodeHook = $_[1]; return }

# ParseUnknownClassHook allows user-defined behavior when the class
# corresponding to the node does not exist or does not implement
# ParseDocumentFragment.
# This method sets ParseUnknownClassHook to the given value.
sub SetUnknownClassHook { our $ParseUnknownClassHook = $_[1]; return }

# Autoparser for Baldr::DtdExtensions::BaldrDtdExtensions objects.
# Constructs a Baldr::DtdExtensions::BaldrDtdExtensions from the given XML element. If the string uses nodes
# with no known classes attached to them, user-defined hooks are run.
# See SetUnknownClassHook and SetInvalidNodeHook.
sub ParseDocumentFragment {
    my ($self, $node) = @_;
    our $ParseUnknownClassHook //= sub {
        confess 'UnknownClassHook not defined for node "', $_[0]->nodeName, '"';
    };
    our $ParseInvalidNodeHook //= sub {
        confess 'ParseInvalidNodeHook not defined for node "', $_[0]->nodeName, '"';
    };
    my $classname = _attrname($node->nodeName);
    $ParseInvalidNodeHook->($node) unless $classname eq 'BaldrDtdExtensions';

    my $newObj = Baldr::DtdExtensions::BaldrDtdExtensions->new;
    $newObj->_suspendAssertions; # object construction

    foreach ($node->nonBlankChildNodes) {
        my $changed_name = _attrname($_->nodeName);
        if (0) { die } # to have elsifs only
        elsif ($changed_name eq 'Namespace') {
            my $o = eval { require Baldr::DtdExtensions::Namespace && Baldr::DtdExtensions::Namespace->can('ParseDocumentFragment') }
                   ? Baldr::DtdExtensions::Namespace->ParseDocumentFragment($_)
                   : $ParseUnknownClassHook->($_);
            $newObj->SetBaldrDtdExtensionsNamespace($o);
        }
        elsif ($changed_name eq 'Coercion') {
            my $o = eval { require Baldr::DtdExtensions::Coercion && Baldr::DtdExtensions::Coercion->can('ParseDocumentFragment') }
                   ? Baldr::DtdExtensions::Coercion->ParseDocumentFragment($_)
                   : $ParseUnknownClassHook->($_);
            $newObj->PushBaldrDtdExtensionsCoercion($o);
        }
        elsif ($changed_name eq 'Constraint') {
            my $o = eval { require Baldr::DtdExtensions::Constraint && Baldr::DtdExtensions::Constraint->can('ParseDocumentFragment') }
                   ? Baldr::DtdExtensions::Constraint->ParseDocumentFragment($_)
                   : $ParseUnknownClassHook->($_);
            $newObj->PushBaldrDtdExtensionsConstraint($o);
        }
    }
    $newObj->_reactivateAssertions; # construction done, check assertions now
    return $newObj;
}


1;
