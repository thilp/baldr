my $member = qr/ value | [A-Z] (?i: [a-z_0-9]* ) /x;

sub _nsc {
    $_[1] && $_[0] ne 'value'
      ? do {
        ( my $nsc = $_[1] ) =~ s/:://xg;
        $nsc . $_[0];
      }
      : $_[0];
}

sub _preprocessConstraint {
    my ($str, $ns) = @_;

    # Inserts "$self->Get" before possible attribute names used as variables.
    $str =~ s/  (?<= \$ )
                ( $member )
        /'self->Get' . _nsc($1, $ns)/exgi;

    # Replaces "." by "->Get" when used as a dereferencer for a $self member.
    $str =~ s/
        (
            \$self
            (?: -> Get$member )+
        )
        \.
        ( $member )
        /"$1->Get" . _nsc($2, $ns)/exg;

    return $str;
}

sub _preprocessErrorMsg {
    my $str = _preprocessConstraint(shift());
    # Interpolates members, since they will be in a string.
    $str =~ s/ ( \$self (?: -> Get$member )+ ) /\@{[ $1 ]}/xg;
    return $str;
}

sub perl {
    my ($self, $ns) = @_;
    return <<"EOF"
    unless (eval {@{[ _preprocessConstraint($self->GetBaldrDtdExtensionsEnforce->Getvalue, $ns) ]}}) {
        die bless {
            from => '@{[ $self->GetBaldrDtdExtensionsClass ]}',
            str => qq^@{[
    _preprocessErrorMsg($self->GetBaldrDtdExtensionsErrormsg->Getvalue) //
    '"' . $self->GetBaldrDtdExtensionsEnforce->Getvalue . '" assertion failed'
            ]}^,
        } => 'BaldrAssertionException';
    }
EOF
}
