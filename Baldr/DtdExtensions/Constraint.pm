# Class automatically generated from a DTD by Baldr
# <https://bitbucket.org/thilp/baldr>
# on Sun Feb  9 05:13:29 2014

package Baldr::DtdExtensions::Constraint;
use 5.010_001;
use strict;
use warnings;

use Carp;
use Scalar::Util 'looks_like_number', 'blessed';




sub new {
    my ($class, %args) = @_;
    return our @ISA ? $class->SUPER::new(%args) : bless \%args => $class;
}


# Not used, because no assertions have been registered for this class.
# These calls will be optimized out by Perl.
sub _check_assertions { 1 };
sub _suspendAssertions { 1 };
sub _reactivateAssertions { 1 };


# Simple getter for "Baldr::DtdExtensions::Enforce".
sub GetBaldrDtdExtensionsEnforce { return $_[0]->{_BaldrDtdExtensionsEnforce} //= undef }

# Simple setter for "Baldr::DtdExtensions::Enforce".
# Returns the updated object.
# Dies if the argument is not a subtype of "Baldr::DtdExtensions::Enforce".
sub SetBaldrDtdExtensionsEnforce {
    croak 'Expecting Baldr::DtdExtensions::Enforce, got undef '
      . '(you might want to use "ForgetBaldrDtdExtensionsEnforce" instead)'
      unless defined $_[1];

    if (!blessed $_[1]) {
        if (ref $_[1]) {
            croak 'Expecting Baldr::DtdExtensions::Enforce, got an unblessed ', ref $_[1];
        }
        else { croak "Expecting Baldr::DtdExtensions::Enforce, got a non-ref \"$_[1]\"" }
    }
    croak 'Expecting Baldr::DtdExtensions::Enforce, got ', ref $_[1] unless $_[1]->isa('Baldr::DtdExtensions::Enforce');

    $_[0]->{_BaldrDtdExtensionsEnforce} = $_[1];
    $_[0]->_check_assertions;

    return $_[0];
}

# Erases the current value for "Baldr::DtdExtensions::Enforce" and replaces it with undef, as if no
# previous values had been set.
# Returns the updated object.
sub ForgetBaldrDtdExtensionsEnforce {
    $_[0]->{_BaldrDtdExtensionsEnforce} = undef;
    $_[0]->_check_assertions;
    return $_[0];
}
# Simple getter for "Baldr::DtdExtensions::Errormsg".
sub GetBaldrDtdExtensionsErrormsg { return $_[0]->{_BaldrDtdExtensionsErrormsg} //= undef }

# Simple setter for "Baldr::DtdExtensions::Errormsg".
# Returns the updated object.
# Dies if the argument is not a subtype of "Baldr::DtdExtensions::Errormsg".
sub SetBaldrDtdExtensionsErrormsg {
    croak 'Expecting Baldr::DtdExtensions::Errormsg, got undef '
      . '(you might want to use "ForgetBaldrDtdExtensionsErrormsg" instead)'
      unless defined $_[1];

    if (!blessed $_[1]) {
        if (ref $_[1]) {
            croak 'Expecting Baldr::DtdExtensions::Errormsg, got an unblessed ', ref $_[1];
        }
        else { croak "Expecting Baldr::DtdExtensions::Errormsg, got a non-ref \"$_[1]\"" }
    }
    croak 'Expecting Baldr::DtdExtensions::Errormsg, got ', ref $_[1] unless $_[1]->isa('Baldr::DtdExtensions::Errormsg');

    $_[0]->{_BaldrDtdExtensionsErrormsg} = $_[1];
    $_[0]->_check_assertions;

    return $_[0];
}

# Erases the current value for "Baldr::DtdExtensions::Errormsg" and replaces it with undef, as if no
# previous values had been set.
# Returns the updated object.
sub ForgetBaldrDtdExtensionsErrormsg {
    $_[0]->{_BaldrDtdExtensionsErrormsg} = undef;
    $_[0]->_check_assertions;
    return $_[0];
}
# Simple getter for "Baldr::DtdExtensions::Class".
sub GetBaldrDtdExtensionsClass { return $_[0]->{_BaldrDtdExtensionsClass} //= undef }

# Simple setter for "Baldr::DtdExtensions::Class". No type checking on the argument.
# Returns the updated object.
sub SetBaldrDtdExtensionsClass {
    $_[0]->{_BaldrDtdExtensionsClass} = $_[1];
    $_[0]->_check_assertions;
    return $_[0];
}

# Remove all blanks before and after the attribute value and returns it.
# (The attribute itself is affected, not only this method's return value.)
sub ChompBaldrDtdExtensionsClass {
    ($_[0]->{_BaldrDtdExtensionsClass} //= undef) =~ s/ \A \s+ | \s+ \z //xg;
    $_[0]->_check_assertions;
    $_[0]->{_BaldrDtdExtensionsClass}
}


my $member = qr/ value | [A-Z] (?i: [a-z_0-9]* ) /x;

sub _nsc {
    my ( $str, $ns ) = @_;
    $ns && $str ne 'value'
      ? do { ( my $nsc = $ns ) =~ s/:://xg; $nsc . $str; }
      : $str;
}

sub _preprocessConstraint {
    my ($str, $ns) = @_;

    # Inserts "$self->Get" before possible attribute names used as variables.
    $str =~ s/  (?<= \$ )
                ( $member )
        /'self->Get' . _nsc($1, $ns)/exgi;

    # Replaces "." by "->Get" when used as a dereferencer for a $self member.
    $str =~ s/
        (
            \$self
            (?: -> Get$member )+
        )
        \.
        ( $member )
        /"$1->Get" . _nsc($2, $ns)/exg;

    return $str;
}

sub _preprocessErrorMsg {
    my $str = _preprocessConstraint(shift());
    # Interpolates members, since they will be in a string.
    $str =~ s/ ( \$self (?: -> Get$member )+ ) /\@{[ $1 ]}/xg;
    return $str;
}

sub perl {
    my ($self, $ns) = @_;
    return <<"EOF"
    unless (eval {@{[ _preprocessConstraint($self->GetBaldrDtdExtensionsEnforce->Getvalue, $ns) ]}}) {
        die bless {
            from => '@{[ $self->GetBaldrDtdExtensionsClass ]}',
            str => qq^@{[
    _preprocessErrorMsg($self->GetBaldrDtdExtensionsErrormsg->Getvalue) //
    '"' . $self->GetBaldrDtdExtensionsEnforce->Getvalue . '" assertion failed'
            ]}^,
        } => 'BaldrAssertionException';
    }
EOF
}


# Helper for the next function.
sub _attrname {
    my ($name) = @_;
    $name =~ s/ [_-]+ ([a-z]) /uc($1)/iegx;
    return ucfirst $name;
}

# ParseInvalidNodeHook allows user-defined behavior when a XML node given as
# argument to ParseDocumentFragment does not correspond to the caller class.
# This method sets ParseInvalidNodeHook to the given value.
sub SetInvalidNodeHook { our $ParseInvalidNodeHook = $_[1]; return }

# ParseUnknownClassHook allows user-defined behavior when the class
# corresponding to the node does not exist or does not implement
# ParseDocumentFragment.
# This method sets ParseUnknownClassHook to the given value.
sub SetUnknownClassHook { our $ParseUnknownClassHook = $_[1]; return }

# Autoparser for Baldr::DtdExtensions::Constraint objects.
# Constructs a Baldr::DtdExtensions::Constraint from the given XML element. If the string uses nodes
# with no known classes attached to them, user-defined hooks are run.
# See SetUnknownClassHook and SetInvalidNodeHook.
sub ParseDocumentFragment {
    my ($self, $node) = @_;
    our $ParseUnknownClassHook //= sub {
        confess 'UnknownClassHook not defined for node "', $_[0]->nodeName, '"';
    };
    our $ParseInvalidNodeHook //= sub {
        confess 'ParseInvalidNodeHook not defined for node "', $_[0]->nodeName, '"';
    };
    my $classname = _attrname($node->nodeName);
    $ParseInvalidNodeHook->($node) unless $classname eq 'Constraint';

    my $newObj = Baldr::DtdExtensions::Constraint->new;
    $newObj->_suspendAssertions; # object construction

    foreach ($node->attributes) {
        my $changed_name = _attrname($_->nodeName);
        if (0) { die } # to have elsifs only
        elsif ($changed_name eq 'Class') { $newObj->SetBaldrDtdExtensionsClass($_->textContent); }
    }
    foreach ($node->nonBlankChildNodes) {
        my $changed_name = _attrname($_->nodeName);
        if (0) { die } # to have elsifs only
        elsif ($changed_name eq 'Enforce') {
            my $o = eval { require Baldr::DtdExtensions::Enforce && Baldr::DtdExtensions::Enforce->can('ParseDocumentFragment') }
                   ? Baldr::DtdExtensions::Enforce->ParseDocumentFragment($_)
                   : $ParseUnknownClassHook->($_);
            $newObj->SetBaldrDtdExtensionsEnforce($o);
        }
        elsif ($changed_name eq 'Errormsg') {
            my $o = eval { require Baldr::DtdExtensions::Errormsg && Baldr::DtdExtensions::Errormsg->can('ParseDocumentFragment') }
                   ? Baldr::DtdExtensions::Errormsg->ParseDocumentFragment($_)
                   : $ParseUnknownClassHook->($_);
            $newObj->SetBaldrDtdExtensionsErrormsg($o);
        }
    }
    $newObj->_reactivateAssertions; # construction done, check assertions now
    return $newObj;
}


1;
