package Baldr::DTDParser;
use 5.010_001;
use strict;
use warnings;

use Carp 'croak';

use Baldr::DTDObjects::AttDef;
use Baldr::DTDObjects::AttlistDecl;
use Baldr::DTDObjects::Children;
use Baldr::DTDObjects::Choice;
use Baldr::DTDObjects::ContentSpec;
use Baldr::DTDObjects::CP;
use Baldr::DTDObjects::DefaultDecl;
use Baldr::DTDObjects::ElementDecl;
use Baldr::DTDObjects::EnumeratedType;
use Baldr::DTDObjects::Mixed;
use Baldr::DTDObjects::Name;
use Baldr::DTDObjects::Seq;
use Baldr::DTDObjects::StringType;
use Baldr::DTDObjects::TokenizedType;

my $XMLDefs;

# elementdecl ::= '<!ELEMENT' S Name S contentspec S? '>'
# contentspec ::= 'EMPTY' | 'ANY' | Mixed | children
# Mixed ::=   '(' S? '#PCDATA' (S? '|' S? Name)* S? ')*'
#           | '(' S? '#PCDATA' S? ')'
# Name ::= NameStartChar (NameChar)*
# NameStartChar ::= ":" | [A-Z] | "_" | [a-z] | [#xC0-#xD6] |
#               [#xD8-#xF6] | [#xF8-#x2FF] | [#x370-#x37D] | [#x37F-#x1FFF] |
#               [#x200C-#x200D] | [#x2070-#x218F] | [#x2C00-#x2FEF] | [#x3001-#xD7FF] |
#               [#xF900-#xFDCF] | [#xFDF0-#xFFFD] | [#x10000-#xEFFFF]
# NameChar ::= NameStartChar | "-" | "." | [0-9] | #xB7 | [#x0300-#x036F] | [#x203F-#x2040]
# children ::= (choice | seq) ('?' | '*' | '+')?
# choice ::= '(' S? cp ( S? '|' S? cp )+ S? ')'
# seq ::= '(' S? cp ( S? ',' S? cp )* S? ')'
# cp ::= (Name | choice | seq) ('?' | '*' | '+')?
# S ::= (#x20 | #x9 | #xD | #xA)+

# AttlistDecl ::= '<!ATTLIST' S Name AttDef* S? '>'
# AttDef ::= S Name S AttType S DefaultDecl
# AttType ::= StringType | TokenizedType | EnumeratedType
# StringType ::= 'CDATA'
# TokenizedType ::= 'ID' | 'IDREF' | 'IDREFS'
# EnumeratedType ::= NotationType | Enumeration
# NotationType ::= 'NOTATION' S '(' S? Name (S? '|' S? Name)* S? ')'
# Enumeration ::= '(' S? Nmtoken (S? '|' S? Nmtoken)* S? ')'
# Nmtoken ::= (NameChar)+
# DefaultDecl ::= '#REQUIRED' | '#IMPLIED' | (('#FIXED' S)? AttValue)
# AttValue ::= '"' ([^<&"] | Reference)* '"'
#           |  "'" ([^<&'] | Reference)* "'"
# Reference ::= EntityRef | CharRef
# EntityRef ::= '&' Name ';'
# CharRef ::= '&#' [0-9]+ ';' | '&#x' [0-9a-fA-F]+ ';'

BEGIN {
    $XMLDefs = qr@

    (?(DEFINE)              # ELEMENTS
        (?<elementdecl>
            <!ELEMENT (?&S)
            (?&Name) (?&S)
            (?&contentspec) (?&S)?
            >
        )
        (?<contentspec> EMPTY | ANY | (?&Mixed) | (?&children) )
        (?<Mixed>
                \( (?&S)? \#PCDATA ( (?&S)? \| (?&S)? (?&Name) )* (?&S)? \) \*
            |   \( (?&S)? \#PCDATA (?&S)? \)
        )
        (?<Name> (?&NameStartChar) (?&NameChar)* )
        (?<NameStartChar>
                : | _ | [A-Za-z\x{C0}-\x{D6}\x{D8}-\x{F6}\x{F8}-\x{2FF}]
            |   [\x{370}-\x{37D}\x{37F}-\x{1FFF}\x{200C}-\x{200D}]
            |   [\x{2070}-\x{218F}\x{2C00}-\x{2FEF}\x{3001}-\x{D7FF}]
            |   [\x{F900}-\x{FDCF}\x{FDF0}-\x{FFFD}\x{10000}-\x{EFFFF}]
        )
        (?<NameChar>
                (?&NameStartChar) | - | \.
            |   [0-9\x{B7}\x{0300}-\x{036F}\x{203F}-\x{2040}]
        )
        (?<children> (?: (?&choice) | (?&seq) )  [?*+]? )
        (?<choice> \( (?&S)? (?&cp) (?: (?&S)? \| (?&S)? (?&cp) )+ (?&S)? \) )
        (?<seq> \( (?&S)? (?&cp) (?: (?&S)? , (?&S)? (?&cp) )* (?&S)? \) )
        (?<cp> (?: (?&Name) | (?&choice) | (?&seq) )  [?*+]? )
        (?<S> [\x{20}\x{9}\x{D}\x{A}]+ )
    )

    (?(DEFINE)              # ATTRIBUTES
        (?<AttlistDecl> <!ATTLIST (?&S) (?&Name) (?&AttDef)* (?&S)? > )
        (?<AttDef> (?&S) (?&Name) (?&S) (?&AttType) (?&S) (?&DefaultDecl) )
        (?<AttType> (?&StringType) | (?&TokenizedType) | (?&EnumeratedType) )
        (?<StringType> CDATA )
        (?<TokenizedType> ID | IDREF | IDREFS )
        (?<EnumeratedType> (?&NotationType) | (?&Enumeration) )
        (?<NotationType>
            NOTATION (?&S)
            \( (?&S)? (?&Name)
            (
                (?&S)? \| (?&S)? (?&Name)
            )*
            (?&S)? \)
        )
        (?<Enumeration>
            \( (?&S)? (?&Nmtoken)
            (
                (?&S)? \| (?&S)? (?&Nmtoken)
            )*
            (?&S)? \)
        )
        (?<Nmtoken> ( (?&NameChar) )+ )
        (?<DefaultDecl>
            \#REQUIRED | \#IMPLIED | ( \#FIXED (?&S) )? (?&AttValue)
        )
        (?<AttValue>
                " ( [^<&"] | (?&Reference) )* "
            |   ' ( [^<&'] | (?&Reference) )* '
        )
        (?<Reference> (?&EntityRef) | (?&CharRef) )
        (?<EntityRef> & (?&Name) ; )
        (?<CharRef> &\# [0-9]+ ; | &\#x [0-9a-fA-F]+ ; )
    )

    @x;
}

sub matches {
    my ( $str, $regexElement ) = @_;
    my $rule = '(?&' . quotemeta($regexElement) . ')';
    return $str =~ / ^ $rule $ $XMLDefs /x;
}

sub parseDecl {
    my $str = shift;
    if ( matches( $str, 'elementdecl' ) ) {
        return parseElementDecl($str);
    }
    elsif ( matches( $str, 'AttlistDecl' ) ) {
        return parseAttlistDecl($str);
    }
    croak "Unknown declaration (neither 'elementdecl' nor 'AttlistDecl')";
}

sub parseAttlistDecl {
    my $str = shift;
    return unless $str =~ / ^ <!ATTLIST (?&S) ((?&Name)) $XMLDefs /x;
    my $o = Baldr::DTDObjects::AttlistDecl->new($1);
    $o->add( parseAttDef($&) ) while $str =~ / (?&AttDef) $XMLDefs /gx;
    return $o;
}

sub parseAttDef {
    my $str = shift;
    return unless $str =~ /
        ^   (?&S)   (?<NAME>    (?&Name)        )
            (?&S)   (?<TYPE>    (?&AttType)     )
            (?&S)   (?<DEFAULT> (?&DefaultDecl) )
        $   $XMLDefs /x;
    return Baldr::DTDObjects::AttDef->new( $+{NAME}, parseAttType( $+{TYPE} ),
        parseDefaultDecl( $+{DEFAULT} )
    );
}

sub parseAttType {
    my $str = shift;
    if ( matches( $str, 'StringType' ) ) {
        return Baldr::DTDObjects::StringType->new;
    }
    elsif ( matches( $str, 'TokenizedType' ) ) {
        return Baldr::DTDObjects::TokenizedType->new($str);
    }
    elsif ( matches( $str, 'EnumeratedType' ) ) {
        if ( matches( $str, 'NotationType' ) ) {
            croak "NOTATION: not yet implemented!";
        }
        else {
            my @c = $str =~ m@ \( (?&S)? ((?&Nmtoken)) $XMLDefs @x or die;
            push @c, $str =~ m@ (?&S)? \| (?&S)? ((?&Nmtoken)) $XMLDefs @xg;
            return Baldr::DTDObjects::EnumeratedType->new( @c );
        }
    }
    return;
}

sub parseDefaultDecl {
    my $str = shift;
    if (index($str, '#FIXED') == 0) {
        if ($str =~ / ((?&AttValue)) $ $XMLDefs /x) {
            return Baldr::DTDObjects::DefaultDecl->new( '#FIXED', $1 );
        }
    }
    return Baldr::DTDObjects::DefaultDecl->new( $str );
}

sub parseElementDecl {
    my $str = shift;
    return unless $str =~ /
        ^ <!ELEMENT         (?&S)
        ( (?&Name) )        (?&S)
        ( (?&contentspec) ) (?&S)?
        > $ $XMLDefs
    /x;
    my ( $name, $cs ) = ( $1, $2 );

    my $o = Baldr::DTDObjects::ElementDecl->new($name);

    if ( $cs eq 'EMPTY' || $cs eq 'ANY' ) {
        $o->contentspec( Baldr::DTDObjects::ContentSpec->new($cs) );
    }
    elsif ( matches( $cs, 'Mixed' ) ) {
        $o->contentspec( parseMixed($cs) );
    }
    else {
        $o->contentspec( parseChildren($cs) );
    }

    return $o;
}

sub parseMixed {
    my $str = shift;
    if ( $str =~ m@ \( (?&S)? \#PCDATA (?&S)? \) $XMLDefs @x ) {
        return Baldr::DTDObjects::Mixed->new;
    }
    my @names = $str =~ m@ \| (?&S)? ((?&Name)) $XMLDefs @xg;
    return Baldr::DTDObjects::Mixed->new(
        map { Baldr::DTDObjects::Name->new($_) }
        grep { defined } @names
    );
}

sub parseChildren {
    my $str = shift;
    return unless $str =~ m@
        (?: (?<CHOICE>(?&choice)) | (?<SEQ>(?&seq)) )  (?<MULT>[?*+]?)
        $XMLDefs
    @x;

    return Baldr::DTDObjects::Children->new(
        $+{MULT} || 1,
        $+{CHOICE} ? parseChoice( $+{CHOICE} ) : parseSeq( $+{SEQ} ),
    );
}

sub parseChoice {
    my $str = shift;
    my @cps = $str =~ m@ \( (?&S)?  ((?&cp)) $XMLDefs @x or return;
    push @cps => ( $str =~ m@ (?&S)? \| (?&S)?  ((?&cp)) $XMLDefs @xg )
      or return;
    $str =~ m@ (?&S)? \) $XMLDefs @x or return;
    my $o = Baldr::DTDObjects::Choice->new;
    $o->add( parseCP($_) ) for grep { defined } @cps;
    return $o;
}

sub parseSeq {
    my $str = shift;
    my @cps = $str =~ m@ \( (?&S)?  ((?&cp)) $XMLDefs @x or return;
    push @cps => ( $str =~ / (?&S)? , (?&S)?  ((?&cp)) $XMLDefs /xg );
    $str =~ m@ (?&S)? \) $XMLDefs @x or return;
    my $o = Baldr::DTDObjects::Seq->new;
    $o->add( parseCP($_) ) for grep { defined } @cps;
    return $o;
}

sub parseCP {
    my $str = shift;
    return unless $str =~ m@
        (?:
                (?<NAME>(?&Name))
            |   (?<CHOICE>(?&choice))
            |   (?<SEQ>(?&seq))
        )
        (?<MULT>[?*+]?)  $XMLDefs
    @x;
    my $o = Baldr::DTDObjects::CP->new( $+{MULT} || 1 );
    if ( $+{NAME} ) {
        $o->value( Baldr::DTDObjects::Name->new( $+{NAME} ) );
    }
    elsif ( $+{CHOICE} ) {
        $o->value( parseChoice( $+{CHOICE} ) );
    }
    else {
        $o->value( parseSeq( $+{SEQ} ) );
    }
    return $o;
}

1;
