package Baldr::CoercionStore;
use 5.010_001;
use strict;
use warnings;

use Carp;

sub new { bless {}, $_[0] }

sub importCoercions {
    my ( $self, $rCoercions ) = @_;
    for my $coer (@$rCoercions) {
        my $code;
        for my $in ( $coer->GetBaldrDtdExtensionsIn ) {
            $code = $in->Getvalue if $in->Getvalue ne '';
            croak 'in coercion, specify code at least once'
              unless defined $code;
            $code =~ s/ \A \h+ | \h+ \z //xg;
            $self->{ $in->GetBaldrDtdExtensionsClass }
              { $in->GetBaldrDtdExtensionsOn || 'value' } =
              { TO => $coer->GetBaldrDtdExtensionsTo, HOW => $code };
        }
    }
    return $self;
}

sub _makeUnaryFunction {
    my %o    = @_;
    my $code = $o{CODE};
    my $name = coercionSubroutineName(%o);
    return <<"EOF";
sub $name {
    return \$_[0] if blessed \$_[0] or ref \$_[0];
    local \$_ = \$_[0];
    $code
}
EOF
}

# Returns the name of the conversion subroutine generated for these particular
# class, class member and target type.
sub coercionSubroutineName {
    my %o = @_;
    my ($class, $member, $to) = @o{qw< CLASS MEMBER TO >};
    return "_Coerce${class}${member}To${to}";
}

# Returns the coercion object (as a hashref) if the required coercion is
# found, or undef.
# Possible argument keys:
#   TO:     To which type the coercion is performed (default: first found)
#   FROM:   From which type the coercion is performed (default: 'value')
#   CLASS:  In which class this coercion takes place (default: first found)
# The TO key is only used to ensure the coercion you get is what you expected.
sub getCoercionObj {
    my ( $self, %o ) = @_;
    $o{FROM} ||= 'value';
    if ( $o{CLASS} ) {
        my $res = $self->{ $o{CLASS} }{ $o{FROM} };
        return unless defined $res;
        if   ( !$o{TO} or $res->{TO} eq $o{TO} ) { return $res }
        else                                     { return }
    }
    else {
        while ( my ( $k, $v ) = each %$self ) {
            next if not exists $v->{ $o{FROM} };
            my $res = $v->{ $o{FROM} };
            next if $o{TO} and $res->{TO} ne $o{TO};
            return $res;
        }
        return;
    }
    die 'unreachable';
}

# Returns the coercion code (as a unary function) if the required coercion is
# found, or undef.
# Possible argument keys:
#   TO:     To which type the coercion is performed (default: first found)
#   FROM:   From which type the coercion is performed (default: 'value')
#   CLASS:  In which class this coercion takes place (default: first found)
# The TO key is only used to ensure the coercion you get is what you expected.
sub getCoercionSubroutine {
    my ( $self, %o ) = @_;
    $o{FROM} ||= 'value';
    if ( $o{CLASS} ) {
        my $res = $self->{ $o{CLASS} }{ $o{FROM} };
        return unless defined $res;
        if ( !$o{TO} or $res->{TO} eq $o{TO} ) {
            return _makeUnaryFunction(
                CLASS  => $o{CLASS},
                MEMBER => $o{FROM},
                TO     => $res->{TO},
                CODE   => $res->{HOW}
            );
        }
        else { return }
    }
    else {
        while ( my ( $k, $v ) = each %$self ) {
            next if not exists $v->{ $o{FROM} };
            my $res = $v->{ $o{FROM} };
            next if $o{TO} and $res->{TO} ne $o{TO};
            return _makeUnaryFunction(
                CLASS  => $k,
                MEMBER => $o{FROM},
                TO     => $res->{TO},
                CODE   => $res->{HOW}
            );
        }
        return;
    }
    die 'unreachable';
}

1;
