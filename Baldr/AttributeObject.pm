package Baldr::AttributeObject;
use 5.010_001;
use strict;
use warnings;

use Carp;

use Baldr::DTDObjects::AttDef;

use constant {
    AO_UKNW => 0, # The origin of this AttributeObject is UnKNoWn.
    AO_ATTR => 1, # This AttributeObject comes from an XML ATTRibute.
    AO_CHLD => 2, # This AttributeObject comes from an XML element (CHiLD).
};

sub new {
    my ( $class, $name, $arity, $type ) = @_;

    croak '"name" parameter required' unless defined $name;
    croak '"arity" parameter required' unless defined $arity;
    croak 'Expected "SCALAR" or "LIST" for "arity", got "', $arity, '"'
      unless $arity eq 'SCALAR' || $arity eq 'LIST';

    my $self = bless {
        name    => _make_attributename($name),
        arity   => $arity,
        default => undef,
        origin  => AO_UKNW,
    } => $class;

    $self->type( $type );

    return $self;
}

# To force a name, prefix it with "!".
sub name {
    my $self = shift;
    $self->{name} = _make_attributename( shift() ) if @_;
    return $self->{name};
}

sub is_scalar { $_[0]->{arity} eq 'SCALAR' }
sub is_list   { $_[0]->{arity} eq 'LIST' }

sub set_scalar { $_[0]->{arity} = 'SCALAR' }
sub set_list   { $_[0]->{arity} = 'LIST' }

sub get_origin { $_[0]->{origin} }
sub set_origin_attr { $_[0]->{origin} = AO_ATTR }
sub set_origin_child { $_[0]->{origin} = AO_CHLD }

sub type {
    my $self = shift;
    if ( my $arg = shift ) {
        croak 'Expected Baldr::AttributeObjectType, got ', ref $arg
          unless $arg->isa('Baldr::AttributeObjectType');
        $self->{type} = $arg;
    }
    return $self->{type};
}

sub default {
    my $self = shift;
    $self->{default} = shift() if @_;
    return $self->{default};
}

sub str {
    '{'
      . ref( $_[0] ) . '|'
      . '"' . $_[0]->name . '",'
      . $_[0]->{arity} . ','
      . $_[0]->type->str . ','
      . ($_[0]->default // '(default undef)') . '}';
}

sub _make_attributename {
    my ($name) = @_;
    return $name if $name =~ s/ ^ ! //x;
    $name =~ s/ [_-]+ ([a-z]) /uc($1)/iegx;
    return ucfirst $name;
}

sub set_coercion {
    my ($self, $to, $code) = @_;
    $self->type( Baldr::AttributeObjectType::Class->new($to) );
    $self->{coerced} = 1;
    return;
}

sub is_coerced { $_[0]->{coerced} }

1;
