package Baldr::ClassIndex;
use 5.010_001;
use strict;
use warnings;

use Carp;

use Baldr::CoercionStore;
use Baldr::ClassObject;
use Baldr::DTDParser;

# Given a ClassObject A of name "a", INHERITANCE->{a} is an arrayref of
# all names of classes that A inherits from.
sub new {
    my ( $class, $logger, $version ) = @_;
    return bless {
        INDEX       => {},         # Associates a classname to a ClassObject
        INHERITANCE => {},
        NAMESPACE   => undef,
        CONSTRAINTS => undef,
        COERCIONS   => Baldr::CoercionStore->new,
        LOGGER      => $logger,
        VERSION     => $version,
    } => $class;
}

sub setExtensions {
    my ( $self, $ext ) = @_;
    if (@_) {
        if ( $ext->isa('Baldr::DtdExtensions::BaldrDtdExtensions') ) {
            $self->{NAMESPACE} = $ext->GetBaldrDtdExtensionsNamespace->Getvalue;
            $self->{CONSTRAINTS} = $ext->GetBaldrDtdExtensionsConstraintRef;
            $self->{COERCIONS}
              ->importCoercions( $ext->GetBaldrDtdExtensionsCoercionRef );
        }
        else {
            croak q{Expected Baldr::DtdExtensions::BaldrDtdExtensions, got },
              ref $ext;
        }
    }
    return;
}

sub getNamespace   { $_[0]->{NAMESPACE} }
sub getConstraints { $_[0]->{CONSTRAINTS} }
sub getCoercions   { $_[0]->{COERCIONS} }

sub makeClassObject {
    my ( $self, $cname ) = @_;
    croak q{Expected an argument, got nothing} unless defined $cname;
    $cname = Baldr::ClassObject::_make_classname($cname);

    return $self->{INDEX}{$cname} if exists $self->{INDEX}{$cname};

    my $c = Baldr::ClassObject->new(
        NAME        => $cname,
        NAMESPACE   => $self->{NAMESPACE},
        CONSTRAINTS => $self->{CONSTRAINTS},
        COERCIONS   => $self->{COERCIONS},
        LOGGER      => $self->{LOGGER},
        FACTORY     => $self,
    );
    $self->{INDEX}{$cname} = $c;
    return $c;
}

sub makeClassObjectFrom {
    my ( $self, $str ) = @_;
    chomp $str;
    my $tree     = Baldr::DTDParser::parseDecl($str);
    my $classobj = $self->makeClassObject( $tree->name );
    $classobj->transformClassObject($tree);
    return $classobj;
}

sub getClassObject {
    my ( $self, $cname ) = @_;
    croak qq{Unknown requested class "$cname"}
      unless exists $self->{INDEX}{$cname};
    return $self->{INDEX}{$cname};
}

sub getInheritanceOf {
    my ( $self, $cname ) = @_;
    return $self->{INHERITANCE}{$cname} //= [];
}

sub setInheritance {
    my ( $self, $child, @parents ) = @_;
    push @{ $self->{INHERITANCE}{$child} } => @parents;
}

sub allClassObjects { values $_[0]->{INDEX} }

1;
