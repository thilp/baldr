package Baldr::ClassObject;
use 5.010_001;
use strict;
use warnings;

use re 'eval';

use Carp;
use Scalar::Util qw< blessed weaken >;

use Baldr::DTDParser;

# For DTDTransformer
use Baldr::ClassObject;
use Baldr::AttributeObject;
use Baldr::AttributeObjectType::Scalar;
use Baldr::AttributeObjectType::Enum;
use Baldr::AttributeObjectType::Tokens;
use Baldr::AttributeObjectType::Class;

use constant FIELD_SEPARATOR => "\n\n";

### Constructor

sub new {
    my ( $class, %o ) = @_;
    my $self = bless {
        NAME        => $o{NAME},
        NS          => $o{NAMESPACE} // '',
        CONSTRAINTS => $o{CONSTRAINTS},
        COERCIONS   => $o{COERCIONS},
        LOG         => $o{LOGGER},
        FACTORY     => $o{FACTORY},
    } => $class;
    weaken( $self->{FACTORY} );
    return $self;
}

### Name formatters

sub ns {
    $_[0]->{NS} && $_[1] ne 'value'
      ? $_[0]->{NS} . '::' . $_[1]
      : $_[1];
}

sub nsc {
    $_[0]->{NS} && $_[1] ne 'value'
      ? do { ( my $nsc = $_[0]->{NS} ) =~ s/:://xg; $nsc . $_[1]; }
      : $_[1];
}

### Basic accessors

sub name {
    my $self = shift;
    $self->{NAME} = _make_classname(shift @_) if @_;
    return $self->{NAME};
}

sub nsName { return $_[0]->ns($_[0]->{NAME}) }

sub collapsedName { return $_[0]->nsc($_[0]->{NAME}) }

sub _inh { $_[0]->{FACTORY}->getInheritanceOf($_[1]) }

sub str {
    '{'
    . ref( $_[0] ) . '|'
    . '"' . $_[0]->name . '",'
    . 'inheritsFrom=' . join('/', @{ $_[0]->_inh($_[0]->name) } ) . ','
    . 'attributes=' . join('/', map { $_->str } $_[0]->attributes)
    . '}'
}

# Inheritance accessors

sub setInheritance {
    my ( $self, %a ) = @_;
    if ( $a{from} ) {
        $self->{FACTORY}->setInheritance( $self->name, @{ $a{from} } );
    }
    if ( $a{by} ) {
        for my $heir ( @{ $a{by} } ) {
            $self->{FACTORY}->setInheritance( $heir, $self->name );
        }
    }
    return $self;
}

sub inherits {
    my ( $self, %a ) = @_;
    my ( $name, $ok ) = ( $self->name, 1 );
    if ( $a{from} ) {
        for my $f ( @{ $a{from} } ) {
            $ok &&= grep { $_ eq $f } @{ $self->{FACTORY}{INHERITANCE}{$name} };
        } continue { return 0 unless $ok }
    }
    if ( $a{by} ) {
        for my $h ( @{ $a{by} } ) {
            $ok &&= grep { $_ eq $name } @{ $self->{FACTORY}{INHERITANCE}{$h} };
        } continue { return 0 unless $ok }
    }
    return $ok;
}

### Methods for class modelisation

sub attributes {
    my $self = shift;
    while ( my $arg = shift @_ ) {
        confess 'Expecting Baldr::AttributeObject, got ', ref $arg
          unless blessed($arg)
          and $arg->isa('Baldr::AttributeObject');
        push @{ $self->{attributes} //= [] } => $arg;
    }
    return @{ $self->{attributes} //= [] };
}

sub scalar_attributes { grep { $_->is_scalar } @{ $_[0]->{attributes} } }
sub list_attributes   { grep { $_->is_list   } @{ $_[0]->{attributes} } }

### Methods for stringification

# IF YOU CHANGE THIS, BE SURE TO CHANGE IT IN EMBEDDED CODE TOO! CF. AUTOPARSE
sub _make_classname {
    my ($name) = @_;
    $name =~ s/ [_-]+ ([a-z]) /uc($1)/iegx;
    return ucfirst $name;
}

# Returns (as a string) the Perl code that implements the described class.

sub perl {
    my $self = shift;

    my $classname = $self->name;

    my $inheritance =
      @{ $self->{FACTORY}{INHERITANCE}{$classname} // [] }
      ? "use parent '"
      . join( "','" => @{ $self->{FACTORY}{INHERITANCE}{$classname} } ) . "';"
      : '';

    my $str .= join FIELD_SEPARATOR,
      $self->_preamble(),
      $inheritance,
      _constructor(),
      $self->_assertions(),
      $self->_coercions(),
      $self->_accessors('single'),
      $self->_accessors('list'),
      $self->inserted(),
      $self->autoparse(),
      _conclusion();

    return $str;
}

### Specialized functions

sub _preamble {
    my $self = shift;
    my $time = localtime;
    my ($name, $version) = ($self->nsName, $self->{FACTORY}{VERSION});
    return <<"EOF";
# Class automatically generated from a DTD by Baldr v$version
# <https://bitbucket.org/thilp/baldr>
# on $time

package $name;
use 5.010_001;
use strict;
use warnings;

use Carp;
use Scalar::Util 'looks_like_number', 'blessed';
EOF
}

sub _assertions {
    my $self  = shift;
    my $cname = $self->name;
    if (
        grep { $_->GetBaldrDtdExtensionsClass eq $cname }
        @{$self->{CONSTRAINTS}}
      )
    {
        return <<"EOF"
# Ability to suspend assertions for a while (and then re-enable them) is
# essential during object-construction, because fields are not initialized at
# the same time.
sub _suspendAssertions {
    \$_[0]->{_assertions_suspended} = 1;
    return;
}
sub _reactivateAssertions {
    \$_[0]->{_assertions_suspended} = 0;
    \$_[0]->_check_assertions;
    return;
}

# Throws a detailed exception if any registered assertion for this class
# evaluates to false. Otherwise, returns 1.
sub _check_assertions {
    my \$self = shift;
    return 1 if \$self->{_assertions_suspended};
@{[
    join '' =>
      map { $_->perl($self->{NS}) }
      grep { $_->GetBaldrDtdExtensionsClass eq $cname }
      @{$self->{CONSTRAINTS}}
]}
    return 1;
}
EOF
    }

    else {
        return <<'EOF'
# Not used, because no assertions have been registered for this class.
# These calls will be optimized out by perl.
sub _check_assertions     { 1 }
sub _suspendAssertions    { 1 }
sub _reactivateAssertions { 1 }
EOF
    }
}

sub _coercions {
    my $self = shift;
    my $str  = "# Coercion subroutines\n";
    for my $attr ( $self->attributes ) {
        next unless $attr->type->isa('Baldr::AttributeObjectType::Class');
        $str .= $self->{COERCIONS}->getCoercionSubroutine(
            CLASS => $self->name,
            FROM  => $attr->name,
            TO    => $attr->type->name
        ) // '';
    }
    return $str;
}

sub _constructor {
    return <<'EOF';
sub new {
    my ($class, %args) = @_;
    return our @ISA ? $class->SUPER::new(%args) : bless \%args => $class;
}
EOF
}

sub inserted {
    my $self = shift;
    ( $self->{inserted} //= '' ) .=
      FIELD_SEPARATOR . join('', @_ )
      if @_;
    return $self->{inserted} // '';
}

sub _conclusion {
    return <<'EOF';
1;
EOF
}

### Generic accessor generator, using the following specialized functions

sub _accessors {
    my ( $self, $kind ) = @_;
    croak "Invalid kind, expecting 'single' or 'list', got '$kind'"
      unless grep { $_ eq $kind } qw( single list );
    my $dispatch = {
        single => {
            agnostic => \&_accessors_single_agnostic,
            scalar   => \&_accessors_single_scalar,
            class    => \&_accessors_single_class,
            enum     => \&_accessors_single_enum,
            tokens   => \&_accessors_single_tokens,
        },
        list => {
            agnostic => \&_accessors_list_agnostic,
            scalar   => \&_accessors_list_scalar,
            class    => \&_accessors_list_class,
            enum     => \&_accessors_list_enum,
            tokens   => \&_accessors_list_tokens,
        },
    };
    my $str        = '';
    my @attributes = do {
        if    ( $kind eq 'single' ) { $self->scalar_attributes }
        elsif ( $kind eq 'list' )   { $self->list_attributes }
    };
    for my $attr (@attributes) {
        $str .= $dispatch->{$kind}{agnostic}->($attr, $self) . "\n";
        $str .= $dispatch->{$kind}{ $attr->type->what }->($attr, $self);
    }

    return $str;
}

### Scalar accessors

sub _accessors_single_agnostic {
    my ( $attr, $self ) = @_;
    my ( $name, $default ) = ( $attr->name, $attr->default );
    my ( $nsName, $nscName ) = ( $self->ns($name), $self->nsc($name) );
    $default = defined $default ? "'$default'" : 'undef';
    return <<"EOF";
# Simple getter for "$nsName".
sub Get$nscName { return \$_[0]->{_$nscName} //= $default }
EOF
}

sub _accessors_single_scalar {
    my ( $attr, $self ) = @_;
    my ( $name, $default ) = ( $attr->name, $attr->default );
    my ( $nsName, $nscName ) = ( $self->ns($name), $self->nsc($name) );
    $default = defined $default ? "'$default'" : 'undef';
    return <<"EOF";
# Simple setter for "$nsName". No type checking on the argument.
# Returns the updated object.
sub Set$nscName {
    \$_[0]->{_$nscName} = \$_[1];
    \$_[0]->_check_assertions;
    return \$_[0];
}

# Remove all blanks before and after the attribute value and returns it.
# (The attribute itself is affected, not only this method's return value.)
sub Chomp$nscName {
    (\$_[0]->{_$nscName} //= $default) =~ s/ \\A \\s+ | \\s+ \\z //xg;
    \$_[0]->_check_assertions;
    \$_[0]->{_$nscName}
}
EOF
}

sub _accessors_single_class {
    my ( $attr, $self ) = @_;
    my ( $name, $type ) = ( $attr->name, $attr->type->name );
    my ( $nsName, $nscName ) = ( $self->ns($name), $self->nsc($name) );
    my $coercionName = $self->{COERCIONS}
      ->coercionSubroutineName( $self->name, $name, $type );
    return <<"EOF";
# Simple setter for "$nsName".
# Returns the updated object.
# Dies if the argument is not a subtype of "$nsName".
sub Set$nscName {
    my \$v = $coercionName(\$_[1]);

    croak 'Expecting $nsName, got undef '
      . '(you might want to use "Forget$nscName" instead)'
      unless defined \$v;

    if (!blessed \$v) {
        if (ref \$v) {
            croak 'Expecting $nsName, got an unblessed ', ref \$v;
        }
        else { croak qq{Expecting $nsName, got a non-ref "\$v"} }
    }

    croak 'Expecting $nsName, got ', ref \$v unless \$v->isa('$nsName');

    \$_[0]->{_$nscName} = \$v;
    \$_[0]->_check_assertions;

    return \$_[0];
}

# Erases the current value for "$nsName" and replaces it with undef, as if no
# previous values had been set.
# Returns the updated object.
sub Forget$nscName {
    \$_[0]->{_$nscName} = undef;
    \$_[0]->_check_assertions;
    return \$_[0];
}
EOF
}

sub _accessors_single_enum {
    my ( $attr, $self ) = @_;
    my ( $name, @values ) = ( $attr->name, $attr->type->values );
    my ( $nsName, $nscName ) = ( $self->ns($name), $self->nsc($name) );
    return <<"EOF";
# Simple setter for "$nsName".
# Returns the updated object.
# Dies if the argument is not one of the following:
#     '@{[ join "'\n#" . q/ / x 5 . "'" => @values ]}'
sub Set$nscName {
    croak 'Expecting a non-ref, got ', ref \$_[1] if ref \$_[1];
    croak 'Expecting "@{[ join q(" or ") => @values ]}", got "', \$_[1], '"'
      if !grep { \$_ eq \$_[1] } ( '@{[ join q(', ') => @values ]}' );

    \$_[0]->{_$nscName} = \$_[1];
    \$_[0]->_check_assertions;

    return \$_[0];
}

# Returns a list of legible values for "$nsName".
sub Legible$nscName { return ( '@{[ join q(', ') => @values ]}' ) }
EOF
}

sub _accessors_single_tokens {
    my ( $attr, $self ) = @_;
    my $name = $attr->name;
    my ($nsName, $nscName) = ($self->ns($name), $self->nsc($name));
    return <<"EOF";
# Simple setter for "$nsName".
# Expecting strings containing a whitespace-separated sequence of identifiers.
# All strings are merged in one token sequence.
# Returns the updated object.
sub Set$nscName {
    my \$self = shift;
    if ( local ( \$_ ) = grep { not defined or ref } \@_ ) {
        if   ( not defined ) { croak 'Expecting a string, got undef' }
        else                 { croak 'Expecting a string, got ', ref }
    }
    \$self->{_$nscName} = [ split /\\s+/ => join ' ' => \@_ ];
    \$self->_check_assertions;
    return \$self;
}
EOF
}

### List accessors

sub _accessors_list_agnostic {
    my ( $attr, $self ) = @_;
    my ( $name, $default ) = ( $attr->name, $attr->default );
    my ( $nsName, $nscName ) = ( $self->ns($name), $self->nsc($name) );
    $default = defined $default ? "'$default'" : 'undef';
    return <<"EOF";
# List getter for "$nsName".
# With no argument, returns all the elements of the list.
# With an argument, and if this argument is a number, returns an element of
# the list by its index (works as expected with negative indexes).
sub Get${nscName} {
    if (defined \$_[1]) {
        croak 'Expecting a numeric index, got "', \$_[1], '"'
          unless looks_like_number(\$_[1]);
        return \$_[0]->{_name}->[\$_[1]] //= $default;
    }
    else {
        return \@{ \$_[0]->{_$nscName} };
    }
}

# ArrayRef getter for "$nsName".
# Returns an array reference on a (shallow) copy of the list.
sub Get${nscName}Ref { my \@list = \@{ \$_[0]->{_$nscName} }; return \\\@list }

# Empties the current list for "$nsName".
# Returns the updated object.
sub Forget$nscName {
    \$_[0]->{_$nscName} = [];
    \$_[0]->_check_assertions;
    return \$_[0];
}

# Emptiness test.
# Returns 0 if the list is empty, 1 otherwise.
sub Has${nscName} { scalar \@{ \$_[0]->{_$nscName} } ? 1 : 0 }

# Counter.
# Returns the number of elements of the list.
sub Count${nscName} { scalar \@{ \$_[0]->{_$nscName} } }

# Pop.
# See &CORE::pop.
sub Pop${nscName} {
    my \$ret = CORE::pop( \@{ \$_[0]->{_$nscName} } );
    \$_[0]->_check_assertions;
    return \$ret;
}

# Shift.
# See &CORE::shift.
sub Shift${nscName} {
    my \$ret = CORE::shift( \@{ \$_[0]->{_$nscName} } );
    \$_[0]->_check_assertions;
    return \$ret;
}
EOF
}

sub _accessors_list_scalar {
    my ( $attr, $self ) = @_;
    my $name = $attr->name;
    my ( $nsName, $nscName ) = ( $self->ns($name), $self->nsc($name) );
    return <<"EOF";
# List setter for "$nsName".
# Clears the attribute's content, then uses the arguments as new elements of
# the list.
sub Set${nscName} {
    my \$self = shift;
    \$self->{_$nscName} = [ \@_ ];
    \$self->_check_assertions;
    return \$self;
}

# ArrayRef setter for "$nsName".
# Replaces the attribute's value with the first argument.
# Other arguments are ignored.
sub Set${nscName}Ref {
    croak 'Expecting an arrayref, got undef' unless defined \$_[1];
    croak 'Expecting an arrayref, got ', ref \$_[1] unless \$_[1] eq 'ARRAY';
    \$_[0]->{_$nscName} = \$_[1];
    \$_[0]->_check_assertions;
    return;
}

# Push.
# See &CORE::push. Does not check arguments' type.
sub Push${nscName} {
    my \$self = shift;
    my \$ret = CORE::push( \@{ \$self->{_$nscName} }, \@_ );
    \$self->_check_assertions;
    return \$ret;
}

# Unshift.
# See &CORE::unshift. Does not check arguments' type.
sub Unshift${nscName} {
    my \$self = shift;
    my \$ret = CORE::unshift( \@{ \$self->{_$nscName} }, \@_ );
    \$self->_check_assertions;
    return \$ret;
}
EOF
}

sub _accessors_list_class {
    my ( $attr, $self ) = @_;
    my ( $name, $type ) = ( $attr->name, $attr->type->name );
    my ( $nsName, $nscName ) = ( $self->ns($name), $self->nsc($name) );
    my $coercionName = $self->{COERCIONS}
      ->coercionSubroutineName( $self->name, $name, $type );
    return <<"EOF";
# List setter for "$nsName".
# Clears the attribute's content, then uses the arguments as new elements of
# the list. Returns the updated object.
# Dies if one of the arguments is not a subtype of "$nsName".
sub Set${nscName} {
    my \$self = shift;
    if ( local ( \$_ ) = grep { !blessed \$_ || !\$_->isa('$nsName') } \@_ ) {
        if ( not defined ) {
            croak 'Expecting $nsName, got undef '
              . '(you might want to use "Forget$nscName" instead)';
        }
        elsif ( not ref ) {
            croak "Expecting $nsName, got a non-ref \\"\$_\\"";
        }
        elsif ( not blessed \$_ ) {
            croak 'Expecting $nsName, got an unblessed ref ', ref;
        }
        else {    # not \$_->isa('$nsName')
            croak 'Expecting $nsName, got ', ref;
        }
    }
    \$self->{_$nscName} = [ \@_ ];
    \$self->_check_assertions;
    return \$self;
}

# ArrayRef setter for "$nsName"
# Replaces the attribute's value with the union of the arguments.
# Dies if one of the arguments is not an arrayref, or if an arrayref contains
# any element that is not of a subtype of "$nsName".
# Returns the updated object.
sub Set${nscName}Ref {
    my \$self = shift;
    my \@final;
    for my \$a ( \@_ ) {
        if ( !ref \$a ) {
            if ( not defined \$a ) {
                croak 'Expecting an arrayref, got undef '
                  . '(you might want to use "Forget$nscName" instead)';
            }
            else { croak 'Expecting an arrayref, got "', \$a, '"' }
        }
        elsif ( local (\$_) = grep { !blessed \$_ || !\$_->isa('$nsName') } \@\$a ) {
            if ( not defined ) {
                croak 'Expecting $nsName, got undef '
                  . '(you might want to use "Forget$nscName" instead)';
            }
            elsif ( not ref ) {
                croak "Expecting $nsName, got a non-ref \\"\$_\\"";
            }
            elsif ( not blessed \$_ ) {
                croak 'Expecting $nsName, got an unblessed ref ', ref;
            }
            else {    # not \$_->isa('$nsName')
                croak 'Expecting $nsName, got ', ref;
            }
        }
        else { push \@final, \@\$a }
    }

    \$self->{_$nscName} = \\\@final;
    \$self->_check_assertions;

    return \$self;
}

# Push
# See &CORE::push. Dies if any argument is not of a subtype of "$nsName".
sub Push${nscName} {
    my \$self = shift;
    if ( local ( \$_ ) = grep { !blessed \$_ || !\$_->isa('$nsName') } \@_ ) {
        if ( not defined ) {
            croak 'Expecting $nsName, got undef '
              . '(you might want to use "Forget$nscName" instead)';
        }
        elsif ( not ref ) {
            croak "Expecting $nsName, got a non-ref \\"\$_\\"";
        }
        elsif ( not blessed \$_ ) {
            croak 'Expecting $nsName, got an unblessed ref ', ref;
        }
        else {    # not \$_->isa('$nsName')
            croak 'Expecting $nsName, got ', ref;
        }
    }
    my \$ret = CORE::push( \@{ \$self->{_$nscName} }, \@_ );
    \$self->_check_assertions;
    return \$ret;
}

# Unshift
# See &CORE::unshift. Dies if any argument is not of a subtype of "$nsName".
sub Unshift${nscName} {
    my \$self = shift;
    if ( local ( \$_ ) = grep { !blessed \$_ || !\$_->isa('$nsName') } \@_ ) {
        if ( not defined ) {
            croak 'Expecting $nsName, got undef '
              . '(you might want to use "Forget$nscName" instead)';
        }
        elsif ( not ref ) {
            croak "Expecting $nsName, got a non-ref \\"\$_\\"";
        }
        elsif ( not blessed \$_ ) {
            croak 'Expecting $nsName, got an unblessed ref ', ref;
        }
        else {    # not \$_->isa('$nsName')
            croak 'Expecting $nsName, got ', ref;
        }
    }
    my \$ret = CORE::unshift( \@{ \$self->{_$nscName} }, \@_ );
    \$self->_check_assertions;
    return \$ret;
}
EOF
}

sub _accessors_list_enum {
    my ( $attr, $self ) = @_;
    my ( $name, @values ) = ( $attr->name, $attr->type->values );
    my ( $nsName, $nscName ) = ( $self->ns($name), $self->nsc($name) );
    return <<"EOF";
# List setter for "$nsName".
# Clears the attribute's content, then uses the arguments as new elements of
# the list. Returns the updated object.
# Dies if one of the arguments are not one of the following:
#     '@{[ join "'\n#" . q/ / x 5 . "'" => @values ]}'
sub Set${nscName} {
    my \$self = shift;
    if ( local (\$_) = grep {
            !defined
         || ref
         || ( \$_ ne '@{[ join "' && \$_ ne '", @values ]}' )
         } \@_ )
    {
        if ( not defined ) {
            croak 'Expecting a string, got undef '
              . '(you might want to use "Forget$nscName" instead)';
        }
        elsif ( ref ) { croak 'Expecting a non-ref, got ', ref }
        else {
            croak 'Expecting "@{[ join q(" or ") => @values ]}", got "',
                \$_, '"'
        }
    }
    \$self->{_$nscName} = [ \@_ ];
    \$self->_check_assertions;
    return \$self;
}

# ArrayRef setter for "$nsName"
# Replaces the attribute's value with the union of the arguments.
# Dies if one of the arguments is not an arrayref, or if an arrayref contains
# any element that is not one of the following:
#     '@{[ join "'\n#" . q/ / x 5 . "'" => @values ]}'
# Returns the updated object.
sub Set${nscName}Ref {
    my \$self = shift;
    my \@final;
    for my \$a ( \@_ ) {
        if ( !ref \$a ) {
            if ( not defined \$a ) {
                croak 'Expecting an arrayref, got undef '
                  . '(you might want to use "Forget$nscName" instead)';
            }
            else { croak 'Expecting an arrayref, got "', \$a, '"' }
        }
        elsif ( local (\$_) = grep {
                !defined
             || ref
             || ( \$_ ne '@{[ join "' && \$_ ne '", @values ]}' )
             } \@\$a )
        {
            if ( not defined ) {
                croak 'Expecting a string, got undef '
                  . '(you might want to use "Forget$nscName" instead)';
            }
            elsif ( ref ) { croak 'Expecting a non-ref, got ', ref }
            else {
                croak 'Expecting "@{[ join q(" or ") => @values ]}", got "',
                    \$_, '"'
            }
        }
        else { push \@final, \@\$a }
    }

    \$self->{_$nscName} = \\\@final;
    \$self->_check_assertions;

    return \$self;
}

# Push
# See &CORE::push.
# Dies if one of the arguments are not one of the following:
#     '@{[ join "'\n#" . q/ / x 5 . "'" => @values ]}'
sub Push${nscName} {
    my \$self = shift;
    if ( local (\$_) = grep {
            !defined
         || ref
         || ( \$_ ne '@{[ join "' && \$_ ne '", @values ]}' )
         } \@_ )
    {
        if ( not defined ) {
            croak 'Expecting a string, got undef '
              . '(you might want to use "Forget$nscName" instead)';
        }
        elsif ( ref ) { croak 'Expecting a non-ref, got ', ref }
        else {
            croak 'Expecting "@{[ join q(" or ") => @values ]}", got "',
                \$_, '"'
        }
    }
    my \$ret = CORE::push( \@{ \$self->{_$nscName} }, \@_ );
    \$self->_check_assertions;
    return \$ret;
}

# Unshift
# See &CORE::unshift.
# Dies if one of the arguments are not one of the following:
#     '@{[ join "'\n#" . q/ / x 5 . "'" => @values ]}'
sub Unshift${nscName} {
    my \$self = shift;
    if ( local (\$_) = grep {
            !defined
         || ref
         || ( \$_ ne '@{[ join "' && \$_ ne '", @values ]}' )
         } \@_ )
    {
        if ( not defined ) {
            croak 'Expecting a string, got undef '
              . '(you might want to use "Forget$nscName" instead)';
        }
        elsif ( ref ) { croak 'Expecting a non-ref, got ', ref }
        else {
            croak 'Expecting "@{[ join q(" or ") => @values ]}", got "',
                \$_, '"'
        }
    }
    my \$ret = CORE::unshift( \@{ \$self->{_$nscName} }, \@_ );
    \$self->_check_assertions;
    return \$ret;
}
EOF
}

sub _accessors_list_tokens {
    my ( $attr, $self ) = @_;
    my $name = $attr->name;
    my ($nsName, $nscName) = ($self->ns($name), $self->nsc($name));
    return <<"EOF";
# List setter for "$nsName".
# Expecting strings containing a whitespace-separated sequence of identifiers.
# Each string is considered one token sequence.
# Returns the updated object.
sub Set$nscName {
    my \$self = shift;
    my \@final;
    if ( local ( \$_ ) = grep { not defined or ref } \@\$a ) {
        if   ( not defined ) { croak 'Expecting a string, got undef' }
        else                 { croak 'Expecting a string, got ', ref }
    }
    \$self->{_$nscName} = [ map { [ split /\\s+/ ] } \@_ ];
    \$self->_check_assertions;
    return \$self;
}

# Push.
# See &CORE::push. Each string is considered one token sequence.
sub Push${nscName} {
    my \$self = shift;
    my \$ret = CORE::push( \@{ \$self->{_$nscName} }, map { [ split /\\s+/ ] } \@_ );
    \$self->_check_assertions;
    return \$ret;
}

# Unshift.
# See &CORE::unshift. Each string is considered one token sequence.
sub Unshift${nscName} {
    my \$self = shift;
    my \$ret = CORE::unshift( \@{ \$self->{_$nscName} }, map { [ split /\\s+/ ] } \@_ );
    \$self->_check_assertions;
    return \$ret;
}
EOF
}

### Autoparsing methods

sub _autoparse_attribute {
    my ( $a, $self ) = @_;
    my $name = $a->name;
    my $nscName = $self->nsc($name);
    return <<"EOF";
        elsif (\$changed_name eq '$name') { \$newObj->Set$nscName(\$_->textContent); }
EOF
}

sub _autoparse_child_single {
    my ( $a, $self ) = @_;
    my ($name, $type) = ($a->name, $a->type);
    my ($nsName, $nscName) = ($self->ns($name), $self->nsc($name));

    if ( $type->isa('Baldr::AttributeObjectType::Class')) {
        return <<"EOF";
        elsif (\$changed_name eq '$name') {
            my \$o = eval { require $nsName && $nsName\->can('ParseDocumentFragment') }
                   ? $nsName\->ParseDocumentFragment(\$_)
                   : \$ParseUnknownClassHook->(\$_);
            \$newObj->Set$nscName(\$o);
        }
EOF
    }
    else {
        return <<"EOF";
        elsif (\$changed_name eq '$name') {
            \$newObj->Set$nscName(\$_->textContent);
        }
EOF
    }
}

sub _autoparse_child_list {
    my ( $a, $self ) = @_;
    my ($name, $type) = ($a->name, $a->type);
    my ($nsName, $nscName) = ($self->ns($name), $self->nsc($name));

    if ( $type->isa('Baldr::AttributeObjectType::Class')) {
        return <<"EOF";
        elsif (\$changed_name eq '$name') {
            my \$o = eval { require $nsName && $nsName\->can('ParseDocumentFragment') }
                   ? $nsName\->ParseDocumentFragment(\$_)
                   : \$ParseUnknownClassHook->(\$_);
            \$newObj->Push$nscName(\$o);
        }
EOF
    }
    else {
        return <<"EOF";
        elsif (\$changed_name eq '$name') {
            \$newObj->Push$nscName(\$_->textContent);
        }
EOF
    }
}

sub _autoparse_child {
    my ( $a, $self ) = @_;
    if ($a->is_scalar) { return _autoparse_child_single($a, $self) }
    else { return _autoparse_child_list($a, $self) }
}

sub autoparse {
    my $self = shift;
    my ( $name, $nsName, $nscName ) =
      ( $self->name, $self->nsName, $self->collapsedName );
    my $str = <<"EOF";
# Helper for the next function.
sub _attrname {
    my (\$name) = \@_;
    \$name =~ s/ [_-]+ ([a-z]) /uc(\$1)/iegx;
    return ucfirst \$name;
}

# ParseInvalidNodeHook allows user-defined behavior when a XML node given as
# argument to ParseDocumentFragment does not correspond to the caller class.
# This method sets ParseInvalidNodeHook to the given value.
sub SetInvalidNodeHook { our \$ParseInvalidNodeHook = \$_[1]; return }

# ParseUnknownClassHook allows user-defined behavior when the class
# corresponding to the node does not exist or does not implement
# ParseDocumentFragment.
# This method sets ParseUnknownClassHook to the given value.
sub SetUnknownClassHook { our \$ParseUnknownClassHook = \$_[1]; return }

# Autoparser for $nsName objects.
# Constructs a $nsName from the given XML element. If the string uses nodes
# with no known classes attached to them, user-defined hooks are run.
# See SetUnknownClassHook and SetInvalidNodeHook.
sub ParseDocumentFragment {
    my (\$self, \$node) = \@_;
    our \$ParseUnknownClassHook //= sub {
        confess 'UnknownClassHook not defined for node "', \$_[0]->nodeName, '"';
    };
    our \$ParseInvalidNodeHook //= sub {
        confess 'ParseInvalidNodeHook not defined for node "', \$_[0]->nodeName, '"';
    };
    my \$classname = _attrname(\$node->nodeName);
    \$ParseInvalidNodeHook->(\$node) unless \$classname eq '$name';

    my \$newObj = $nsName\->new;
    \$newObj->_suspendAssertions; # object construction

EOF
    if (grep { $_->get_origin == Baldr::AttributeObject::AO_ATTR } $self->attributes) {
        $str .= <<"EOF";
    foreach (\$node->attributes) {
        my \$changed_name = _attrname(\$_->nodeName);
        if (0) { die } # to have elsifs only
EOF
        # All XML attributes
        for my $a ( grep { $_->get_origin == Baldr::AttributeObject::AO_ATTR }
            $self->attributes )
        {
            $str .= _autoparse_attribute( $a, $self );
        }
        $str .= <<"EOF";
    }
EOF
    }

    if (
        grep {
            $_->get_origin == Baldr::AttributeObject::AO_UKNW && $_->name eq 'value'
        } $self->attributes
      )
    {
        $str .= <<"EOF";
    \$newObj->Setvalue(\$_->textContent);
EOF
    }

    if (grep { $_->get_origin == Baldr::AttributeObject::AO_CHLD } $self->attributes) {
        $str .= <<"EOF";
    foreach (\$node->nonBlankChildNodes) {
        my \$changed_name = _attrname(\$_->nodeName);
        if (0) { die } # to have elsifs only
EOF

        # All XML elements (children)
        for my $a (grep { $_->get_origin == Baldr::AttributeObject::AO_CHLD } $self->attributes) {
            $str .= _autoparse_child($a, $self);
        }

        $str .= <<"EOF";
    }
EOF
    }

    $str .= <<"EOF";
    \$newObj->_reactivateAssertions; # construction done, check assertions now
    return \$newObj;
}
EOF

    return $str;
}

###### DTDTransformer ########################################################

my $IMPLICIT_NAMING;
BEGIN { $IMPLICIT_NAMING = {} }

sub transformClassObject {
    my ( $self, $decl ) = @_;

    if ( $decl->isa('Baldr::DTDObjects::AttlistDecl') ) {
        my @attributes =
          map { $self->transformAttDef($_) } $decl->list;
        $self->attributes(@attributes);
    }
    elsif ( $decl->isa('Baldr::DTDObjects::ElementDecl') ) {
        $self->transformElementDecl($decl);
    }
    else { croak ref($_) . ': not supported yet' }

    return;
}

sub transformAttDef {
    my ( $self, $attdef ) = @_;
    my $o = Baldr::AttributeObject->new( $attdef->name, 'SCALAR' );
    $o->set_origin_attr;

    # Type
    for ( $attdef->type ) {
        if ( $_->isa('Baldr::DTDObjects::StringType') ) {
            $o->type( Baldr::AttributeObjectType::Scalar->new );
        }
        elsif ( $_->isa('Baldr::DTDObjects::EnumeratedType') ) {
            $o->type( Baldr::AttributeObjectType::Enum->new( $_->list ) );
        }
        elsif ( $_->isa('Baldr::DTDObjects::TokenizedType') ) {
            $o->type(
                $_->type eq 'ID' || $_->type eq 'IDREF'
                ? Baldr::AttributeObjectType::Scalar->new
                : Baldr::AttributeObjectType::Tokens->new
            );
        }
        else { croak ref($_) . ': not supported yet' }
    }

    # Default value
    for ( $attdef->default ) {
        $o->default( $_->value ) if $_->is_fixed || $_->is_bare;
    }

    # Coercion
    my $coer = $self->{COERCIONS}
        ->getCoercionObj( CLASS => $self->name, FROM => $attdef->name );
    $o->set_coercion( $coer->{TO} ) if $coer;

    return $o;
}

sub transformElementDecl {
    my ( $self, $elementdecl ) = @_;
    my $classobj = $self->{FACTORY}->makeClassObject( $elementdecl->name );

    for ( $elementdecl->contentspec ) {
        if ( $_->type eq 'EMPTY' ) { }    # nothing to do
        elsif ( $_->isa('Baldr::DTDObjects::Children') ) {
            my $attlist = $self->_transformCP( $_, $elementdecl->name );
            $attlist = [ map { $_->set_origin_child; $_ } @$attlist ];
            $classobj->attributes(@$attlist);
        }
        elsif ( $_->isa('Baldr::DTDObjects::Mixed') ) {
            {
                my $pcdata = Baldr::AttributeObject->new( '!value', 'SCALAR' );
                $pcdata->type( Baldr::AttributeObjectType::Scalar->new );

                # Coercion
                my $coer =
                  $self->{COERCIONS}
                  ->getCoercionObj( CLASS => $classobj->name );
                $pcdata->set_coercion( $coer->{TO} ) if $coer;
                $classobj->attributes($pcdata);
            }
            $classobj->attributes(
                map {
                    my $attr = Baldr::AttributeObject->new( $_->value, 'LIST' );
                    $attr->set_origin_child;
                    my $co = $self->{FACTORY}->makeClassObject( $_->value );
                    $attr->type(
                        Baldr::AttributeObjectType::Class->new( $co->name ) );
                    $attr
                } $_->names
            );
        }
        else { croak( ( ref $_ ? ref $_ : $_ ) . ': not supported yet' ) }
    }

    return $classobj;
}

# *** TRANSLATION DTD -> POO ***
#
#   PATTERN-MATCHING:
#
#  1. E( a [1?] )    -> E uses a
#  2. E( a [*+] )    -> E lists a
#
#  3. E( a | b )[1?] -> E uses an abstract E', and a and b "are" E'
#  4. E( a | b )[+*] -> E lists an abstract E', and a and b "are" E'
#
#  5. E( a , b )[1?] -> E uses a and b
#  6. E( a , b )[+*] -> E lists a and b
#
#   ALGORITHM:
#
# Seq or Choice?
#   Seq: multiplicity?
#       SCALAR: 5
#       LIST:   6
#   Choice: alternative?
#       no: multiplicity?
#           SCALAR: 1
#           LIST:   2
#       yes: multiplicity?
#           SCALAR: 3
#           LIST:   4

# $decl:  A DTDObjects::Children or DTDObjects::CP.
# $ename: Name of the current elementdecl (the "E" in the above table).
#
# Walks $decl to translate it from a DTD point-of-view to a POO point-of-view.
#
# Returns (in an arrayref) AttributeObjects associated to the highest-level
# classes that were created/used while transforming $decl.
# If in cases #3 or #4, *and* if E(E|…), the class corresponding to $ename is
# modified accordingly.
sub _transformCP {
    my ( $self, $decl, $ename ) = @_;
    croak 'Expected Baldr::DTDObjects::CP or Baldr::DTDObjects::Children, '
      . 'got a non-ref'
      unless defined $decl;
    croak 'Expected Baldr::DTDObjects::CP or Baldr::DTDObjects::Children, got ',
      ref $decl
      unless $decl->isa('Baldr::DTDObjects::CP')
      || $decl->isa('Baldr::DTDObjects::Children');

    # Un-encapsulate CP
    my ( $node, $m ) = ( $decl->value, $decl->multiplicity );

    if ( $node->isa('Baldr::DTDObjects::Name') ) {
        return $self->_one_class_arrayref( $node->value, $m );
    }

    elsif ( $node->isa('Baldr::DTDObjects::Seq') ) {    # Seq: 5 or 6
        my @final;
        for my $e ( $node->list ) {
            my $attlist = $self->_transformCP( $e, $ename . 'Parcel' );
            map { $_->set_list } @$attlist if _is_list($m);
            push @final, @$attlist;
        }
        return \@final;
    }

    elsif ( $node->isa('Baldr::DTDObjects::Choice') ) {

        if ( $node->list == 1 ) {    # only one element: 1 or 2
            my $attlist = $self->_transformCP( [ $node->list ]->[0], $ename );
            map { $_->set_list } @$attlist if _is_list($m);
            return $attlist;
        }
        else {                       # 3 or 4
            my @final;
            for my $e ( $node->list ) {
                my $attlist = $self->_transformCP( $e, $ename );
                push @final, @$attlist;
            }
            my ($abstract) = @{ $self->_make_inheritance( \@final, $ename ) };
            $abstract->set_list if _is_list($m);
            return [$abstract];
        }

    }

    else { croak ref($_) . ': not yet supported' }
}

sub _one_class_arrayref {
    my ( $self, $name, $multiplicity ) = @_;
    my $co = $self->{FACTORY}->makeClassObject($name);
    return [
        Baldr::AttributeObject->new(
            $co->name,
            _is_scalar($multiplicity) ? 'SCALAR' : 'LIST',
            Baldr::AttributeObjectType::Class->new( $co->name )
        )
    ];
}

sub _is_scalar { $_[0] eq '1' || $_[0] eq '?' }
sub _is_list   { $_[0] eq '+' || $_[0] eq '*' }

# $attlist: An [ AttributeObject+ ] such as the ones returned by transformCP.
# $ename: The (class-) name of the element under which we are working.
#
# Returns a new [ AttributeObject ] which represents $attlist from a POO
# point-of-view, that is that:
#   - directly returns its argument if @$attlist == 1;
#   - generates an abstract class above each class corresponding to $attlist's
#   elements, and returns this abstract class as an AttributeObject
#   otherwise.
sub _make_inheritance {
    my ( $self, $attlist, $ename ) = @_;
    confess 'Unexpected empty list' unless @$attlist;

    if ( my ($a) =
        grep { !$_->type->isa('Baldr::AttributeObjectType::Class') } @$attlist )
    {
        confess 'List contains some non-Baldr::AttributeObjectType::Class '
          . 'elements, first of all: "'
          . $a . '"';
    }

    return $attlist if @$attlist == 1;

    my $abstract =
      $self->{FACTORY}->makeClassObject( 'Abstract'
          . Baldr::ClassObject::_make_classname($ename)
          . ( $IMPLICIT_NAMING->{$ename} // '' ) );

    if ( $IMPLICIT_NAMING->{$ename} ) { $IMPLICIT_NAMING->{$ename}++ }
    else                              { $IMPLICIT_NAMING->{$ename} = 'B' }

    my $name = $abstract->name;

    # If an element in $attlist, say "A", has LIST multiplicity, we must first
    # create another class, say "AList", which "lists" "A" and has SCALAR
    # multiplicity, so that the inheritance is clean.
    for my $i ( 0 .. $#$attlist ) {
        if ( $attlist->[$i]->is_scalar ) {
            my $c = $self->{FACTORY}->makeClassObject( $attlist->[$i]->name )
              ->setInheritance( from => [$name] );
        }
        else {
            my $newclass = $self->{FACTORY}
              ->makeClassObject( $attlist->[$i]->name . 'List' );
            $newclass->attributes( $attlist->[$i] );
            $newclass->setInheritance( from => [$name] );

            $attlist->[$i] =
              Baldr::AttributeObject->new( $newclass->name, 'SCALAR',
                Baldr::AttributeObjectType::Class->new( $newclass->name ),
              );
        }
    }
    return $self->_one_class_arrayref( $name, '1' );
}

1;
